import matplotlib.pyplot as plt
import numpy as np

from pipeline.utils.read import read_velo
from pipeline.input_representation import FVRepresentation, BEVRepresentation


if __name__ == '__main__':
    frame = read_velo(9)

    # get fv representation
    fv_rep = FVRepresentation()
    fv = fv_rep.encode(frame)

    # get bev representation
    bev_rep = BEVRepresentation(x_range=(0, 70.4), y_range=(-40, 40), z_range=(-3, 2))
    bev = bev_rep.encode(frame)

    # plot fv representation
    fig = plt.figure(1)
    fig.suptitle("Front View LiDAR Input Representation")
    splt = plt.subplot(3, 1, 1)
    splt.set_title("height")
    plt.imshow(-fv[:, :, 0], cmap=plt.get_cmap('jet'))
    splt = plt.subplot(3, 1, 2)
    splt.set_title("distance")
    plt.imshow(fv[:, :, 1], cmap=plt.get_cmap('jet'))
    splt = plt.subplot(3, 1, 3)
    splt.set_title("intensity")
    plt.imshow(fv[:, :, 2], cmap=plt.get_cmap('jet'))

    # plot bev representation
    fig = plt.figure(2)
    fig.suptitle("Bird-Eye View LiDAR Input Representation")
    bev_sum = np.sum(bev, axis=-1)
    plt.imshow(bev_sum, cmap=plt.get_cmap('jet'))

    plt.show()