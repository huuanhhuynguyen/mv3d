import matplotlib.pyplot as plt

from pipeline.utils.coordinates import get_bev
from pipeline.utils.read import read_velo


if __name__ == '__main__':
    frame = read_velo(8)

    bev = get_bev(frame)

    plt.imshow(bev, cmap=plt.get_cmap('Greys'))
    plt.show()