import numpy as np
import matplotlib.pyplot as plt

from pipeline.utils.coordinates import get_corners_3D, get_bev
from pipeline.utils.plot import plot_obbs_bev, plot_obbs_fv, plot_3D_obbs_camview, plot_obbs_camview_2D
from pipeline.utils.read import read_calib_matrices, read_gt_obbs, read_velo
from pipeline.input_representation import FVRepresentation
from pipeline.multi_view_projections import box3d_to_fv, box3d_to_camera3d, box3d_to_camera2d, box3d_to_bev


if __name__ == '__main__':
    frame_no = 7
    M = read_calib_matrices(frame_no)
    obbs = read_gt_obbs(M['Tr_velo_to_cam'], frame_no)  # xyz, thetaY in camera coordinates

    # plot bev
    frame = read_velo(frame_no)
    bev = get_bev(frame)
    plt.figure(1)
    plt.imshow(bev, cmap=plt.get_cmap('Greys'))

    # plot obbs on bev
    boxes3d_corners = get_corners_3D(obbs)
    cor_bev = box3d_to_bev(boxes3d_corners)
    plt = plot_obbs_bev(plt, cor_bev)

    # plot fv
    fv_rep = FVRepresentation()
    fv = fv_rep.encode(frame)
    plt.figure(2)
    plt.imshow(np.sum(fv, axis=-1), cmap=plt.get_cmap('jet'))

    # plot obbs on fv
    corners_i_fv = box3d_to_fv(boxes3d_corners)
    plt = plot_obbs_fv(plt, corners_i_fv)

    # plot image
    plt.figure(3)
    img = plt.imread('../kitti_mini_dataset/img/{:06d}.png'.format(frame_no))
    plt.imshow(img)

    # plot obbs(3D) on image
    cor_img = box3d_to_camera3d(boxes3d_corners, M['Tr_velo_to_cam'], M['P2'], M['R0_rect'])
    plt = plot_3D_obbs_camview(plt, cor_img)

    # plot image
    plt.figure(4)
    img = plt.imread('../kitti_mini_dataset/img/{:06d}.png'.format(frame_no))
    plt.imshow(img)

    # plot obbs(2D) on image
    cor_img_2D = box3d_to_camera2d(boxes3d_corners, M['Tr_velo_to_cam'], M['P2'], M['R0_rect'])
    plt = plot_obbs_camview_2D(plt, cor_img_2D)
    plt.show()