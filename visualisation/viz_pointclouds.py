import mayavi.mlab as mlab
import numpy as np

from pipeline.utils.read import read_calib_matrices, read_gt_obbs, read_velo
from pipeline.utils.coordinates import get_corners_3D


def draw_pointcloud(velo):
    """
    Plots 3D lidar point cloud data
    Ref: https://github.com/ronrest/point_clouds/blob/master/mayavi_viz.py
    :param velo:
    :return:
    """
    x, y, z, r = np.split(velo, 4, axis=-1)

    # create a mlab figure
    fig = mlab.figure(bgcolor=(0, 0, 0), size=(640, 360))

    # plot lidar points
    mlab.points3d(
                    x, y, z, r,           # reflectivity used for shading
                    mode="point",         # How to render each point {'point', 'sphere' , 'cube' }
                    colormap='spectral',  # 'bone', 'copper',
                    scale_factor=100,     # scale of the points
                    line_width=10,        # Scale of the line, if any
                    figure=fig,
                )

    # draw centre location of lidar
    mlab.points3d(0, 0, 0, color=(1, 1, 1), mode='sphere', scale_factor=0.2)

    # draw x,y,z axes of lidar
    axes = np.array([
        [2., 0., 0., 0.],
        [0., 2., 0., 0.],
        [0., 0., 2., 0.],
    ], dtype=np.float64)
    mlab.plot3d([0, axes[0, 0]], [0, axes[0, 1]], [0, axes[0, 2]], color=(1, 0, 0), figure=fig)
    mlab.plot3d([0, axes[1, 0]], [0, axes[1, 1]], [0, axes[1, 2]], color=(0, 1, 0), figure=fig)
    mlab.plot3d([0, axes[2, 0]], [0, axes[2, 1]], [0, axes[2, 2]], color=(0, 0, 1), figure=fig)

    return fig


def draw_gt_boxes3d(gt_boxes3d, fig, color=(0,1,0), line_width=1, draw_text=True, text_scale=(0.5,0.5,0.5), color_list=None):
    """
    Draws 3D bounding boxes on lidar point cloud data
    Ref: https://github.com/kuixu/kitti_object_vis/blob/master/viz_util.py
    :param gt_boxes3d: (n, 8, 3) bounding box corners
    :param fig: mayavi figure
    :param color: (R, G, B) value tuple in range (0, 1), box line colour
    :param line_width: box line width
    :param draw_text: boolean, if true, write box indices beside boxes
    :param text_scale: three number tuple
    :param color_list: a list of RGB tuple, if not None, overwrite color
    :return: fig, mayavi figure with plotted boxes
    """

    num = len(gt_boxes3d)
    for n in range(num):
        b = gt_boxes3d[n]
        if color_list is not None:
            color = color_list[n]
        if draw_text: mlab.text3d(b[4, 0], b[4, 1], b[4, 2], '%d' % n, scale=text_scale, color=color, figure=fig)
        for k in range(0,4):
            # Horizontal plane - bottom
            i, j = k, (k+1) % 4
            mlab.plot3d([b[i, 0], b[j, 0]],
                        [b[i, 1], b[j, 1]],
                        [b[i, 2], b[j, 2]], color=color, line_width=line_width, figure=fig)
            # Horizontal plane - top
            i , j = k+4, (k+1) % 4 + 4
            mlab.plot3d([b[i, 0], b[j, 0]],
                        [b[i, 1], b[j, 1]],
                        [b[i, 2], b[j, 2]], color=color, line_width=line_width, figure=fig)
            # Vertical plane
            i, j = k, (k+1) % 4 + 4
            mlab.plot3d([b[i, 0], b[j, 0]],
                        [b[i, 1], b[j, 1]],
                        [b[i, 2], b[j, 2]], color=color, line_width=line_width, figure=fig)

    return fig


if __name__ == '__main__':
    frame_no = 8
    frame = read_velo(frame_no)
    fig = draw_pointcloud(frame)

    M = read_calib_matrices(frame_no)
    boxes3d = read_gt_obbs(M['Tr_velo_to_cam'], frame_no)
    boxes3d_corners = get_corners_3D(boxes3d)
    fig = draw_gt_boxes3d(boxes3d_corners, fig)

    azimuth, elevation, distance, focal_point = 120, 30, 70, [0, 0, 0]
    mlab.view(azimuth, elevation, distance, focal_point, figure=fig)
    mlab.show()