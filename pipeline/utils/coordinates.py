import numpy as np
import tensorflow as tf
from math import pi

# =============================== READ ME ===================================
# There are two coordinate systems within the scope of this project
# KITTI coordinate system: x = rightwards, y = downwars, z = forwards
# ISO coordinate system: x = forwards, y = leftwards and z = upwards.
# ===========================================================================

# -------------------------- default global parameters -----------------------------------------------------------------
X_MIN, X_MAX = 0.0, 70.4
Y_MIN, Y_MAX = -40.0, 40.0
Z_MIN, Z_MAX = -3.0, 2.0
DX = DY = 0.1  # x,y-grid resolution
DZ = 1  # z-resolution
X = int((X_MAX - X_MIN) / DX)
Y = int((Y_MAX - Y_MIN) / DY)
Z = int((Z_MAX - Z_MIN) / DZ)


# -------------------------- 2D and 3D corners of bounding boxes -------------------------------------------------------

def get_corners_3D(obbs, forward_backward_equivalence=False):
    """
    Get coordinates of the eight corners of 3D OBBs in ISO coordinate system.
    :param obbs: (n_boxes, 7), columns represent theta, x, y, z, l, w, h
    :param forward_backward_equivalence: bool, if true, the returned corners of two opposite obbs (e.g. theta=0
    and theta=pi) will be the same in order.
    :return: (n_boxes, 8, 3), xyz-coordinates of eight corners.
    """

    # 8 corners starting from bottom-left-below and counter clockwise
    #
    # 1(6)______2(7)
    #   |  /\  |
    #   | EGO  |
    #   |______|
    #  4(5)    3(8)
    theta, x, y, z, l, w, h = np.split(obbs, 7, axis=-1)

    if forward_backward_equivalence:
        theta = np.arctan(np.tan(theta))    # normalize to range [-pi/2, pi/2]

    corners_b = np.array([(l/2, w/2, h*0), (l/2, -w/2, h*0), (-l/2, -w/2, h*0), (-l/2, w/2, h*0),
                          (-l/2, w/2, h), (l/2, w/2, h), (l/2, -w/2, h), (-l/2, -w/2, h)])

    corners_b = np.swapaxes(corners_b, 1, 2)

    rot = np.array([[np.cos(theta), np.sin(theta)],
                    [-np.sin(theta), np.cos(theta)]]).squeeze(3)
    rot = np.insert(rot, 2, 0, axis=0)
    rot = np.insert(rot, 2, 0, axis=1)
    rot[-1, -1] = 1
    rot = np.swapaxes(rot, 0, 2)

    origin = np.stack([x, y, z], axis=-1).squeeze(1)

    corners_w = np.matmul(rot, corners_b).squeeze(3) + origin  # coordinates in ego (world) frame
    return np.swapaxes(corners_w, 0, 1)


def get_corners_3D_tf(obbs, forward_backward_equivalence=False):
    """
    Get coordinates of the eight corners of 3D OBBs in ISO coordinate system - Tensorflow version.
    :param obbs: (batch_size, n_boxes, 7), columns represent theta, x, y, z, l, w, h
    :param forward_backward_equivalence: bool, if true, the returned corners of two opposite obbs (e.g. theta=0
    and theta=pi) will be the same in order.
    :return: (batch_size, n_boxes, 8, 3), xyz-coordinates of eight corners.
    """
    # 8 corners starting from bottom-left-below and counter clockwise
    #
    # 1(6)______2(7)
    #   |  /\  |
    #   | EGO  |
    #   |______|
    #  4(5)    3(8)
    theta, x, y, z, l, w, h = tf.split(obbs, 7, axis=2)

    if forward_backward_equivalence:
        theta = tf.atan(tf.tan(theta)) # normalize to range [-pi/2, pi/2]

    corners_b = tf.convert_to_tensor([(l/2, w/2, h*0), (l/2, -w/2, h*0), (-l/2, -w/2, h*0), (-l/2, w/2, h*0),
                                      (-l/2, w/2, h), (l/2, w/2, h), (l/2, -w/2, h), (-l/2, -w/2, h)], dtype=tf.float32)

    corners_b = tf.transpose(corners_b, [0, 2, 3, 1, 4])

    rot = tf.squeeze(tf.convert_to_tensor([[tf.cos(theta), tf.sin(theta)],
                                        [-tf.sin(theta), tf.cos(theta)]]), 4)

    n_boxes = tf.shape(obbs)[1]
    batch_size = tf.shape(obbs)[0]

    # adding a last row of 0's
    row = tf.zeros([1, 2, batch_size, n_boxes], dtype=tf.float32)
    rot = tf.concat([rot, row], 0)
    # adding last column of 0, 0, 1
    col = tf.reshape(tf.convert_to_tensor([0., 0., 1.]), [-1, 1])
    col = tf.tile(tf.expand_dims(tf.expand_dims(col, 2), 3), [1, 1, batch_size, n_boxes])

    rot = tf.concat([rot, col], 1)
    rot = tf.transpose(rot, [2, 3, 1, 0])
    rot = tf.tile(tf.expand_dims(rot, 0), [8, 1, 1, 1, 1])

    origin = tf.squeeze(tf.stack([x, y, z], axis=-1), 2)
    origin = tf.tile(tf.expand_dims(origin, 0), [8, 1, 1, 1])

    corners_w = tf.squeeze(tf.matmul(rot, corners_b), 4) # coordinates in ego (world) frame
    corners_w = corners_w + origin

    return tf.transpose(corners_w, [1, 2, 0, 3])


def get_corners_2D(obbs, forward_backward_equivalence=False):
    """
    Get coordinates of the four corners of 3D OBBs in ISO coordinate system. Four corners are seen from the bird-eye
    view (i.e. z dimension is ignored)
    :param obbs: (n_boxes, 7), columns represent theta, x, y, z, l, w, h
    :param forward_backward_equivalence: bool, if true, the returned corners of two opposite obbs (e.g. theta=0
    and theta=pi) will be the same in order.
    :return: (n_boxes, 4, 2), xy-coordinates of four corners.
    """
    theta, x, y, l, w = bev_params(obbs)

    if forward_backward_equivalence:
        theta = np.arctan(np.tan(theta))    # normalize to range [-pi/2, pi/2]

    # corners in the body frame
    corners_b = np.stack(((l/2, w/2), (l/2, -w/2),
                          (-l/2, -w/2), (-l/2, w/2)))
    corners_b = np.transpose(corners_b, [0, 2, 1, 3])  # (4, n_boxes, 2, 1)

    # origin of the body frame in the world frame
    origin_b = np.stack((x, y), axis=-1)
    origin_b = np.swapaxes(origin_b, 1, 2)  # (n_boxes, 2, 1)

    # transform from body to world frame
    s = np.sin(theta)
    c = np.cos(theta)
    rot = np.stack([np.stack([c, s], axis=-1),
                    np.stack([-s, c], axis=-1)], axis=-1).squeeze(1)  # (n_boxes, 2, 2)
    corners_w = np.matmul(rot, corners_b) + origin_b
    corners_w = corners_w.squeeze(3)

    return corners_w


def get_corners_2D_tf(obbs, forward_backward_equivalence=False):
    """
    Get coordinates of the four corners of 3D OBBs in ISO coordinate system. Four corners are seen from the bird-eye
    view (i.e. z dimension is ignored) - Tensorflow version
    :param obbs: (batch_size, n_boxes, 7), columns represent theta, x, y, z, l, w, h
    :param forward_backward_equivalence: bool, if true, the returned corners of two opposite obbs (e.g. theta=0
    and theta=pi) will be the same in order.
    :return: (batch_size, n_boxes, 4, 2), xy-coordinates of four corners.
    """
    theta, x, y, l, w = bev_params_tf(obbs)

    if forward_backward_equivalence:
        theta = tf.atan(tf.tan(theta))    # normalize to range [-pi/2, pi/2]

    # corners in the body frame
    corners_b = tf.convert_to_tensor(((l/2, w/2), (l/2, -w/2),
                                    (-l/2, -w/2), (-l/2, w/2)), dtype=tf.float32)

    corners_b = tf.transpose(corners_b, [0, 2, 3, 1, 4])  # (4, batch_size, n_boxes, 2, 1)

    # origin of the body frame in the world frame
    origin_b = tf.stack([x, y], axis=-1)
    origin_b = tf.transpose(origin_b, [0, 1, 3, 2]) # (batch_size, n_boxes, 2, 1)
    origin_b = tf.tile(tf.expand_dims(origin_b, 0), [4, 1, 1, 1, 1])

    # transform from body to world frame
    s = tf.sin(theta)
    c = tf.cos(theta)

    rot = tf.stack([tf.stack([c, s], axis=-1),
                    tf.stack([-s, c], axis=-1)], axis=-1) # (batch_size, n_boxes, 2, 2)
    rot = tf.squeeze(rot, 2)
    rot = tf.tile(tf.expand_dims(rot, 0), [4, 1, 1, 1, 1])

    corners_w = tf.matmul(rot, corners_b)
    corners_w = tf.squeeze(corners_w + origin_b, 4)

    return tf.transpose(corners_w, [1, 0, 2, 3])


# ----------------------------- bird-eye view --------------------------------------------------------------------------

def get_corners_bev(obbs):
    """
    Get coordinates of the four corners of OBBs projected on the BEV (i.e. height and z are ignored) in the ISO
    coordinate system.
    :param obbs: (n_boxes, 7), columns represent theta, x, y, z, l, w, h
    :return: (n_boxes, 4, 2), coordinates of four corners.
    """
    theta, x, y, l, w = bev_params(obbs)

    # normalizes to range [-pi/2, pi/2] to not distinguish two opposite theta values
    # the advantage is that the returned 4 corners of two opposite (e.g. theta1 = -theta2) bounding boxes will align.
    theta = np.arctan(np.tan(theta))
    corners_b = np.array([(l/2, w/2), (l/2, -w/2), (-l/2, -w/2), (-l/2, w/2)])  # coordinates in body frame
    corners_b = np.swapaxes(corners_b, 1, 2)

    rot = np.array([[np.cos(theta), np.sin(theta)],
                    [-np.sin(theta), np.cos(theta)]]).squeeze(3)
    rot = np.swapaxes(rot, 0, 2)

    origin = np.stack([x, y], axis=-1).squeeze(1)

    corners_w = np.matmul(rot, corners_b).squeeze(3) + origin  # coordinates in ego (world) frame
    return np.swapaxes(corners_w, 0, 1)


def get_corners_bev_tf(obbs):
    """
    Get coordinates of the four corners of OBBs projected on the BEV (i.e. height and z are ignored) in the ISO
    coordinate system - Tensorflow version.
    :param obbs: (batch_size, n_boxes, 7), columns represent theta, x, y, z, l, w, h
    :return: (batch_size, n_boxes, 4, 2), coordinates of four corners.
    """
    theta, x, y, l, w = bev_params_tf(obbs)

    # normalizes to range [-pi/2, pi/2] to not distinguish two opposite theta values
    # the advantage is that the returned 4 corners of two opposite (e.g. theta1 = -theta2) bounding boxes will align.
    theta = tf.atan(tf.tan(theta))
    corners_b = tf.convert_to_tensor([(l/2, w/2), (l/2, -w/2),
                                      (-l/2, -w/2), (-l/2, w/2)], dtype=tf.float32)  #coordinates in body frame
    corners_b = tf.transpose(corners_b, [0, 2, 3, 1, 4])

    rot = tf.squeeze(tf.convert_to_tensor([[tf.cos(theta), tf.sin(theta)],
                                          [-tf.sin(theta), tf.cos(theta)]], dtype=tf.float32), 4)
    rot = tf.transpose(rot, [2, 3, 1, 0])
    rot = tf.tile(tf.expand_dims(rot, 0), [4, 1, 1, 1, 1])

    origin = tf.squeeze(tf.stack([x, y], axis=-1), 2)
    origin = tf.tile(tf.expand_dims(origin, 0), [4, 1, 1, 1])

    # coordinates in ego (world) frame
    corners_w = tf.squeeze(tf.matmul(rot, corners_b), 4)
    corners_w = corners_w + origin

    return tf.transpose(corners_w, [1, 2, 0, 3])


def get_bev(frame, x_min=X_MIN, x_max=X_MAX, y_min=Y_MIN, y_max=Y_MAX, dx=DX, dy=DY):
    """
    :param frame: (n_points, 4), lidar frame (4 params are x, y, z, and reflectance).
    :param x_min: float, min x value of all points represented on the BEV.
    :param x_max: float, max x value of all points represented on the BEV.
    :param y_min: float, min y value of all points represented on the BEV.
    :param y_max: float, max y value of all points represented on the BEV.
    :param dx: float, x resolution.
    :param dy: float, y resolution.
    :return: (X, Y), a BEV array that can be displayed by matplotlib.pyplot
    """
    x, y, _, refl = frame.T

    # remove out of range points
    point = np.array([x, y])
    in_x_range = np.logical_and(x_min <= x, x <= x_max)
    in_y_range = np.logical_and(y_min <= y, y <= y_max)
    in_range = np.logical_and(in_x_range, in_y_range)
    x, y = point[:, in_range]

    # initialize bev array
    X = int((x_max - x_min) / dx)
    Y = int((y_max - y_min) / dy)
    bev = np.ones((X, Y))

    # fill values to bev array
    i_x, i_y = xy_to_bev_array_index(x, y, x_min, x_max, y_min, y_max, dx, dy)
    bev[i_x, i_y] = bev[i_x, i_y] - refl[in_range]

    return bev


def xy_to_bev_array_index(x, y, x_min=X_MIN, x_max=X_MAX, y_min=Y_MIN, y_max=Y_MAX, dx=DX, dy=DY):
    """
    Translate x, y of points in ISO coordinate system into the indices of the bev array.
    For example, x_min = -5, dx = 0.1, then if x = [-3, 0.05], i_x = [20, 50].
    :param x: (N,), array of x coordinates.
    :param y: (N,), array of y coordinates.
    :param x_min: float, min x value over the bev.
    :param x_max: float, max x value over the bev.
    :param y_min: float, min y value over the bev.
    :param x_max: float, max y value over the bev.
    :param dx: float, x resolution.
    :param dy: float, y resolution.
    :return i_x, i_y: Tuple(array, array).
    """
    X = int((x_max - x_min) / dx)
    Y = int((y_max - y_min) / dy)

    i_x = ((x - x_min) / dx).astype(np.int)
    i_y = ((y - y_min) / dy).astype(np.int)
    return X-i_x-1, Y-i_y-1


def xy_to_bev_array_index_tf(x, y, x_min=X_MIN, x_max=X_MAX, y_min=Y_MIN, y_max=Y_MAX, dx=DX, dy=DY):
    """
    Translate x, y of points in ISO coordinate system into the indices of the bev array - Tensorflow version.
    For example, x_min = -5, dx = 0.1, then if x = [-3, 0.05], i_x = [20, 50].
    :param x: (N,), array of x coordinates.
    :param y: (N,), array of y coordinates.
    :param x_min: float, min x value over the bev.
    :param x_max: float, max x value over the bev.
    :param y_min: float, min y value over the bev.
    :param x_max: float, max y value over the bev.
    :param dx: float, x resolution.
    :param dy: float, y resolution.
    :return i_x, i_y: Tuple(array, array).
    """
    X = int((x_max - x_min) / dx)
    Y = int((y_max - y_min) / dy)

    i_x = tf.cast(((x - x_min) / dx), dtype=tf.int32)
    i_y = tf.cast(((y - y_min) / dy), dtype=tf.int32)

    return X-i_x-1, Y-i_y-1


def bev_params(boxes):
    """ Returns theta, center, length and width of the given 2D or 3D bounding boxes.
    :param obbs: (n_boxes, 7), columns represent theta, x, y, z, l, w, h
           obbs: (n_boxes, 5), columns represent theta, x, y, l, w
    :return: theta, x, y, l, w
    """
    boxes = np.array(boxes)
    n_params = boxes.shape[-1]
    if n_params == 7:
        theta, x, y, _, l, w, _ = np.split(boxes, 7, axis=-1)
    elif n_params == 5:
        theta, x, y, l, w = np.split(boxes, 5, axis=-1)
    else:
        raise ValueError("Only support 2D OBB with 5 params (theta, cx, cy, l, w) "
                         "or 3D OBB with 7 params (theta, cx, cy, cz, l, w, h)")
    return theta, x, y, l, w


def bev_params_tf(boxes):
    """ Returns theta, center, length and width of the given 2D or 3D bounding boxes - Tensorflow version.
    :param obbs: (batch_size, n_boxes, 7), columns represent theta, x, y, z, l, w, h
           obbs: (batch_size, n_boxes, 5), columns represent theta, x, y, l, w
    :return: theta, x, y, l, w
    """
    # boxes = tf.convert_to_tensor(boxes)
    n_params = boxes.shape[2]

    if n_params == 7:
        theta, x, y, _, l, w, _ = tf.split(boxes, 7, axis=2)
    elif n_params == 5:
        theta, x, y, l, w = tf.split(boxes, 5, axis=2)
    else:
        raise ValueError("Only support 2D OBB with 5 params (theta, cx, cy, l, w) "
                         "or 3D OBB with 7 params (theta, cx, cy, cz, l, w, h)")
    return theta, x, y, l, w

# ----------------------------- Coordinate transformation --------------------------------------------------------------

def to_ISO(Tr_velo_cam, *args, points3d=None, thetaY=None):
    """
    Converts kitti coordinates into coordinates in the ISO frame.
    :param Tr_velo_cam: (4, 4), transformation matrix, ISO to kitti frame
    :param *args: to enforce the user to give the argument explicitly as keyword
    :param points3d: (n_points, 3), x, y, z points in kitti frame
    :param thetaY: (n_points, 1), angles about the y-axis in the kitti frame
    :return: x_y_z (n_points, 3), xyz-coordinates in ISO frame, theta (n_points, 1) yaw angle in ISO frame
    """
    if points3d is None and thetaY is None:
        raise ValueError(
            "Either points3d or theta argument has to be given explicitly (e.g. to_ISO(T, points3d=something)))")

    if points3d is not None:
        x_k, y_k, z_k = np.split(points3d, 3, axis=1)
        Tr_cam_velo = np.linalg.inv(Tr_velo_cam) #to get kitti to ISO matrix
        points = np.transpose(np.stack((x_k, y_k, z_k, np.ones((x_k.shape[0], 1))), axis=1).squeeze(2))
        points_iso = np.dot(Tr_cam_velo, points)

        x = np.reshape(points_iso[0, :], (-1, 1))
        y = np.reshape(points_iso[1, :], (-1, 1))
        z = np.reshape(points_iso[2, :], (-1, 1))

        return np.concatenate([x, y, z], axis=1)

    if thetaY is not None:
        theta = -thetaY - np.pi/2

        return theta


def to_ISO_tf(Tr_velo_cam, *args, points3d=None, thetaY=None):
    """
    Converts kitti coordinates into coordinates in the ISO frame - Tensorflow version.
    :param Tr_velo_cam: (4, 4), transformation matrix, ISO to kitti frame
    :param *args: to enforce the user to give the argument explicitly as keyword
    :param points3d: (n_points, 3), x, y, z points in kitti frame
    :param thetaY: (n_points, 1), angles about the y-axis in the kitti frame
    :return: x_y_z (n_points, 3), xyz-coordinates in ISO frame, theta (n_points, 1) yaw angle in ISO frame
    """
    if points3d is None and thetaY is None:
        raise ValueError(
            "Either points3d or theta argument has to be given explicitly (e.g. to_ISO(T, points3d=something)))")

    if points3d is not None:
        x_k, y_k, z_k = tf.split(points3d, 3, axis=1)

        Tr_cam_velo = tf.matrix_inverse(Tr_velo_cam) #to get kitti to ISO matrix
        ones = tf.ones_like(x_k)
        points = tf.squeeze(tf.stack([x_k, y_k, z_k, ones], axis=1), 2)
        points = tf.transpose(points, [1, 0])

        points_iso = tf.matmul(Tr_cam_velo, points)

        x = tf.reshape(points_iso[0, :], [-1, 1])
        y = tf.reshape(points_iso[1, :], [-1, 1])
        z = tf.reshape(points_iso[2, :], [-1, 1])

        return tf.concat([x, y, z], axis=1)

    if thetaY is not None:
        theta = -thetaY - pi/2

        return theta


def to_KITTI(Tr_velo_cam, *args, points3d=None, thetaZ=None):
    """
    Converts ISO coordinates into the kitti coordinates frame.
    :param Tr_velo_cam: (4, 4), transformation matrix, ISO to kitti frame
    :param *args: to enforce the user to give the argument explicitly as keyword
    :param points3d: (n_points, 3), x, y, z points in ISO frame
    :param thetaZ: (n_points, 1), angles about the z-axis in the ISO frame
    :return: x_y_z (n_points, 3) xyz-coordinates in kitti frame, thetaY (n_points, 1) yaw angle in kitti frame
    """
    if points3d is None and thetaZ is None:
        raise ValueError(
            "Either points3d or theta argument has to be given explicitly (e.g. to_KITTI(T, points3d=something)))")

    if points3d is not None:
        x, y, z = np.split(points3d, 3, axis=1)
        points = np.transpose(np.stack((x, y, z, np.ones((x.shape[0], 1))), axis=1).squeeze())
        points_kitti = np.dot(Tr_velo_cam, points)

        x_k = np.reshape(points_kitti[0, :], (-1, 1))
        y_k = np.reshape(points_kitti[1, :], (-1, 1))
        z_k = np.reshape(points_kitti[2, :], (-1, 1))

        return np.concatenate([x_k, y_k, z_k], axis=1)

    if thetaZ is not None:
        thetaY_k = -thetaZ - np.pi/2

        return thetaY_k


def to_KITTI_tf(Tr_velo_cam, *args, points3d=None, thetaZ=None):
    """
    Converts ISO coordinates into the kitti coordinates frame - Tensorflow version.
    :param Tr_velo_cam: (4, 4), transformation matrix, ISO to kitti frame
    :param *args: to enforce the user to give the argument explicitly as keyword
    :param points3d: (n_points, 3), x, y, z points in ISO frame
    :param thetaZ: (n_points, 1), angles about the z-axis in the ISO frame
    :return: x_y_z (n_points, 3) xyz-coordinates in kitti frame, thetaY (n_points, 1) yaw angle in kitti frame
    """
    if points3d is None and thetaZ is None:
        raise ValueError(
            "Either points3d or theta argument has to be given explicitly (e.g. to_KITTI(T, points3d=something)))")

    if points3d is not None:
        x, y, z = tf.split(points3d, 3, axis=1)

        ones = tf.ones_like(x)
        points = tf.squeeze(tf.stack([x, y, z, ones], axis=1), 2)
        points = tf.transpose(points, [1, 0])

        points_kitti = tf.matmul(Tr_velo_cam, points)

        x_k = tf.reshape(points_kitti[0, :], [-1, 1])
        y_k = tf.reshape(points_kitti[1, :], [-1, 1])
        z_k = tf.reshape(points_kitti[2, :], [-1, 1])

        return tf.concat([x_k, y_k, z_k], axis=1)

    if thetaZ is not None:
        thetaY_k = -thetaZ - pi/2

        return thetaY_k

# ----------------------------- Conversion from 3D to 2D boxes ---------------------------------------------------------
def to_bev_boxes(boxes3d):
    """
    Convert a numpy array of 3D boxes in KITTI coordinates into 2D boxes in BEV coordinates.
    :param boxes3d: (..., 7), 3D boxes
    :return: (..., 5), 2D BEV boxes
    """
    theta, x, y, z, l, w, h = np.split(boxes3d, 7, axis=-1)
    return np.concatenate([theta, x, y, l, w], axis=-1)


def to_bev_boxes_tf(boxes3d):
    """
    Convert a tensor of 3D boxes in KITTI coordinates into 2D boxes in BEV coordinates.
    :param boxes3d: (..., 7), 3D boxes
    :return: (..., 5), 2D BEV boxes
    """
    theta, x, y, z, l, w, h = tf.split(boxes3d, 7, axis=2)
    return tf.concat([theta, x, y, l, w], axis=2)


def convert_to_diagonal_corners(rois):
    """
    Converts 4 corners into 2 diagonal corner points (x_min, y_min, x_max, y_max)
    :param rois: (batch_size, n_boxes, 4, 2) 2D box corners
    :return: (batch_size, n_boxes, 4) Diagonal corners of 2D box
    """
    min_corner = tf.reduce_min(rois, axis=-2)
    max_corner = tf.reduce_max(rois, axis=-2)

    return tf.concat([min_corner, max_corner], axis=-1)


def reformat_rois(rois):
    """
    Reformat the ROIs to feed ROI Pooling Operation
    Note: Please make sure that every batch contains same number of ROIs
    :param rois: (batch_size, n_rois, 4, 2) ROIs 4 corner points
    :return: (batch_size * n_rois, 5) ROIs of the format (batch_id, y1, x1, y2, x2)
    """
    n_boxes_per_batch = tf.shape(rois)[1]
    batch_size = tf.shape(rois)[0]

    batch_idx = tf.reshape(tf.range(0, batch_size, 1), [-1, 1])
    batch_idx = tf.reshape(tf.tile(batch_idx, [1, n_boxes_per_batch]),[-1, 1])
    batch_idx = tf.cast(batch_idx, tf.float32)

    diagonal_corners = convert_to_diagonal_corners(rois)
    tiled_diagonal_corners = tf.reshape(diagonal_corners, [-1, 4])

    x1, y1, x2, y2 = tf.split(tiled_diagonal_corners, 4, -1)
    reformatted_roi = tf.concat([batch_idx, y1, x1, y2, x2], axis=1)

    return reformatted_roi