import numpy as np
import rtree.index
from shapely.affinity import rotate, translate
from shapely.geometry import Polygon

from pipeline.utils.coordinates import get_corners_2D, bev_params

# ========================== IoU calculation of OBBs using Shapely library ==================================
# Goal: calculate a NxM matrix that store IoUs between M boxes (e.g. gt boxes) and N boxes (e.g. top k boxes)
# Method 1: Vectorize using numpy and calculate one pair by one pair.
# Method 2: Using R-Tree to only calculate pairs that have intersection.
# Finally, analyse the time efficiency between these two methods over different values of (M, N).
# ===========================================================================================================


def iou_np(boxesA, boxesB):
    """
    Calculate the IoU for every pair of OBBs in the two arrays. The time efficiency is optimized by numpy vectorization.
    :param boxesA: (M, 5)
    :param boxesB: (N, 5), array of boxes. Each box has 5 params theta, cx, cy, l, w in the order.
    :return iou: (M, N), Array whose element i, j is the IoU for boxesA[i] and boxesB[j].
    """
    # generate two arrays of Shapely.Polygon
    cornersA = get_corners_2D(boxesA)
    cornersB = get_corners_2D(boxesB)
    cornersA = np.swapaxes(cornersA, 0, 1)
    cornersB = np.swapaxes(cornersB, 0, 1)
    polysA = np.array(tuple(Polygon(corner) for corner in cornersA))
    polysB = np.array(tuple(Polygon(corner) for corner in cornersB))

    # calculate the iou matrix
    iou = np.array(tuple(tuple(
        a.intersection(b).area/a.union(b).area for b in polysB) for a in polysA))
    return iou


def iou_rtree(boxesA, boxesB):
    """
    Calculate the IoU for every pair of OBBs in the two arrays. The time efficiency is optimized by using R-Tree.
    Source: https://codereview.stackexchange.com/questions/204017/intersection-over-union-for-rotated-rectangles
    :param boxesA: (M, 5)
    :param boxesB: (N, 5), array of boxes. Each box has 5 params theta, cx, cy, l, w in the order.
    :return iou: (M, N), Array whose element i, j is the IoU for boxesA[i] and boxesB[j].
    """
    M = len(boxesA)
    N = len(boxesB)
    if M > N:
        # More memory-efficient to compute it the other way round and transpose.
        return iou_rtree(boxesB, boxesA).T

    # Convert rects_a to shapely Polygon objects.
    polys_a = [_rect_polygon(b) for b in boxesA]

    # Build a spatial index for rects_a.
    index_a = rtree.index.Index()
    for i, a in enumerate(polys_a):
        index_a.insert(i, a.bounds)

    # Find candidate intersections using the spatial index.
    iou = np.zeros((M, N))
    for j, box in enumerate(boxesB):
        b = _rect_polygon(box)
        for i in index_a.intersection(b.bounds):
            a = polys_a[i]
            intersection_area = a.intersection(b).area
            if intersection_area:
                iou[i, j] = intersection_area / a.union(b).area

    return iou


def _rect_polygon(box):
    """
    Return a shapely Polygon describing the rectangle with centre at (x, y) and the given width and height, rotated by
    angle quarter-turns.
    Source: https://codereview.stackexchange.com/questions/204017/intersection-over-union-for-rotated-rectangles
    """
    theta, x, y, l, w = bev_params(box[None, :])
    l, w = l / 2, w / 2
    p = Polygon([(-l, -w), (l, -w), (l, w), (-l, w)])
    return translate(rotate(p, theta/np.pi*180), x, y)


