import numpy as np
import cv2

def plot_obbs_bev(plt, corners, colour='r'):
    """
    :param corners: (n_boxes, 4, 2), four corners of boxes on the BEV in the order that no two opposite corners are next
    to each other.
    :return: plt instance with plotted boxes.
    """
    if len(corners.shape) == 2:
        corners = corners.reshape([1, 4, 2])

    for corners_one_box in corners:
        plt = _plot_obb_bev(plt, corners_one_box, colour)

    return plt


def _plot_obb_bev(plt, corners, colour='r'):
    for (x1, y1), (x2, y2) in zip(corners, np.roll(corners, 1, axis=0)):
        plt.plot([y1, y2], [x1, x2], colour)
    return plt


def plot_obbs_fv(plt, corners, colour='r'):
    """
    :param corners: (n_boxes, 4, 2), four corners of boxes on the FV in the order that no two opposite corners are next
    to each other.
    :return: plt instance with plotted boxes.
    """
    if len(corners.shape) == 2:
        corners = corners.reshape([1, 4, 2])

    for corners_one_box in corners:
        plt = _plot_obb_fv(plt, corners_one_box, colour)

    return plt


def _plot_obb_fv(plt, corners, colour='r'):
    for (x1, y1), (x2, y2) in zip(corners, np.roll(corners, 1, axis=0)):
        plt.plot([x1, x2], [y1, y2], colour)
    return plt


def plot_obbs_camview_2D(plt, corners, colour='r'):
    """
    :param corners: (n_boxes, 4, 2), four corners of boxes on the Camera view in the order that no two opposite corners are next
    to each other.
    :return: plt instance with plotted boxes.
    """
    if len(corners.shape) == 2:
        corners = corners.reshape([1, 4, 2])

    for corners_one_box in corners:
        plt = _plot_obb_camview_2D(plt, corners_one_box, colour)

    return plt


def _plot_obb_camview_2D(plt, corners, colour='r'):
    for (x1, y1), (x2, y2) in zip(corners, np.roll(corners, 1, axis=0)):
        plt.plot([x1, x2], [y1, y2], colour)
    return plt


def plot_3D_obbs_camview(plt, corners, colour='r'):
    """
    :param corners: (n_boxes, 8, 3), eight corners of 3D boxes, which was projected to the camera view (ISO coordinate
    system), in the order that no two opposite corners are next to each other. Thus, 2 params are z (upwards) and
    y (leftwards).
    :return: plt instance with plotted boxes.
    """
    if len(corners.shape) == 2:
        corners = corners.reshape([1, 8, 3])

    for corners_one_box in corners:
        plt = _plot_obb_camview(plt, corners_one_box, colour)

    return plt


def _plot_obb_camview(plt, corners, colour='r'):
    # draw horizontal planes
    corners_h = corners[:4, :]
    for (u1, v1), (u2, v2) in zip(corners_h, np.roll(corners_h, 1, axis=0)):
        plt.plot([u1, u2], [v1, v2], colour)

    corners_h = corners[4:, :]
    for (u1, v1), (u2, v2) in zip(corners_h, np.roll(corners_h, 1, axis=0)):
        plt.plot([u1, u2], [v1, v2], colour)

    # draw vertical planes
    corners_v = np.stack([corners[6:7, :], corners[5:6, :]], axis=0).squeeze(1)
    corners_v = np.concatenate([corners[:2, :], corners_v], axis=0)
    for (u1, v1), (u2, v2) in zip(corners_v, np.roll(corners_v, 1, axis=0)):
        plt.plot([u1, u2], [v1, v2], colour)

    corners_v = np.stack([corners[4:5, :], corners[7:, :]], axis=0).squeeze(1)
    corners_v = np.concatenate([corners[2:4, :], corners_v], axis=0)
    for (u1, v1), (u2, v2) in zip(corners_v, np.roll(corners_v, 1, axis=0)):
        plt.plot([u1, u2], [v1, v2], colour)

    return plt