from os.path import join, abspath
import numpy as np
from pipeline.utils.coordinates import to_ISO


_ALL_CATEGORIES = {"Car", "Pedestrian", "Van", "Truck",
                   "Person_sitting", "Cyclist", "Tram", "Misc"}
_DATA_DIR = abspath(join(__file__, '../../..', 'kitti_mini_dataset'))
_CALIB_DIR = join(_DATA_DIR, 'calib')
_LABEL_DIR = join(_DATA_DIR, 'label')
_LIDAR_DIR = join(_DATA_DIR, 'lidar')


def read_calib_matrices(frame):
    """
    Read projection matrices of the given frame and return them in a dictionary.
    """
    calib_file = join(_CALIB_DIR, "{:06d}.txt".format(frame))
    with open(calib_file, "r") as f:
        lines = f.read().splitlines()
    lines = [line.split() for line in lines]
    Mat = {line[0][:-1]: list(map(float, line[1:])) for line in lines if line}
    for key, val in Mat.items():
        if key.startswith('P'):
            val = np.array(val).reshape(3, 4)
        else:
            if key == 'R0_rect':
                val = np.array(val).reshape(3, 3)
                val = np.insert(val, 3, 0, axis=1)  # insert a column to the last position and fill with 0's
            else:
                val = np.array(val).reshape(3, 4)
            val = np.insert(val, 3, 0, axis=0)  # insert a row to the last position and fill with 0's
            val[-1, -1] = 1
        Mat[key] = val
    return Mat


def read_gt_obbs(Tr_velo_cam, frame, categories=_ALL_CATEGORIES):
    """
    Read ground truth oriented bounding boxes of the given frame from the dataset
    and convert it to ISO coordinate system.
    """
    gt_file = join(_LABEL_DIR, "{:06d}.txt".format(frame))
    with open(gt_file, "r") as f:
        lines = f.read().splitlines()
    lines = [line.split() for line in lines]

    obbs = np.array([list(map(float, line[8:])) for line in lines if line[0] in categories])

    if obbs.size != 0:
        h, w, l, x, y, z, thetaY = np.split(obbs, 7, axis=-1)
        x_y_z = to_ISO(Tr_velo_cam=Tr_velo_cam, points3d=np.concatenate([x, y, z], axis=1))
        theta = to_ISO(Tr_velo_cam=Tr_velo_cam, thetaY=thetaY)
        obbs = np.concatenate([theta, x_y_z, l, w, h], axis=1)

    return obbs


def read_velo(frame_no):
    file = join(_LIDAR_DIR, "{:06d}.bin".format(frame_no))
    return np.fromfile(file, dtype=np.float32).reshape(-1, 4)
