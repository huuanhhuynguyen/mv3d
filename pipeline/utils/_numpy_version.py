# ========================================== READ ME ===================================================================
# This module is for the ease of development in tensorflow. Each function in this module is the numpy version of some
# tensorflow functionality in our pipeline.
#
# User of the project: doesn't need to care about this module.
# Developer: (optionally) first implement a numpy version here and translate it into tensorflow in the pipeline.
# ======================================================================================================================
import numpy as np
from pipeline.utils.coordinates import to_bev_boxes


def matcher_np(anchors, gt_boxes_3d):
    """
    Classify anchors as pos, neg, ignored and assign gt boxes to pos anchors.
        - Pos anchors are anchors that have distance to gt box center less than 1.5 meter.
        - Neg anchors are anchors that have distance to gt box center greater than or equal 1.5 meter.
        - Ignored anchors are anchors that are neither pos or neg.
    :param anchors: (batch_size, n_anchors, 2)
    :param gt_boxes_3d: (batch_size, ?, 7) where ? is n_boxes.
    :return pos_anchors: (batch_size, n_pos_anchors, 2) positive anchors
            neg_anchors: (batch_size, n_neg_anchors, 2) negative anchors
            gt_boxes_pos: (batch_size, n_pos_anchors, 7) gt boxes where each box corresponds to each positive anchor
    """
    batch_size, n_anchors = anchors.shape[:2]
    n_boxes = gt_boxes_3d.shape[1]

    # 3d boxes to 2d
    gt_boxes = to_bev_boxes(gt_boxes_3d)

    # Find distance matrix
    dist_mat = distance_calculator_np(anchors, gt_boxes)            # (batch_sz, n_anchors, n_boxes)

    # For each anchor, only keep the distance to the closest box
    dists_min = np.min(dist_mat, axis=-1)  # (batch_sz, n_anchors,)
    dists_min = np.tile(dists_min[:, :, None], [1, 1, n_boxes])              # (batch_sz, n_anchors, n_boxes)
    dist_mat[dist_mat > dists_min] = 1000  # very large

    # Find distance mask
    mask_dist = dist_mat < 1.5  # 1.5 meter radius

    # Tile gt_boxes
    gt_boxes_3d = np.tile(gt_boxes_3d[:, None, :, :], [1, n_anchors, 1, 1])  # (batch_sz, n_anchors, n_boxes, 5)

    # Get positive, negative anchors and gt_boxes_pos
    mask_anchor = np.any(mask_dist, axis=-1)                               # (batch_sz, n_anchors)
    anchors_ixs = np.tile(np.arange(n_anchors)[None, :], [batch_size, 1])    # (batch_sz, n_anchors)
    pos_anchors_ixs = anchors_ixs[mask_anchor]                               # (batch_sz x ?,)
    neg_anchors_ixs = anchors_ixs[np.logical_not(mask_anchor)]               # (batch_sz x ?,)
    gt_boxes_pos = gt_boxes_3d[mask_dist]                                  # (batch_sz x ?, 7)

    # Reshape
    pos_anchors_ixs = np.reshape(pos_anchors_ixs, [batch_size, -1])          # (batch_sz, ?)
    neg_anchors_ixs = np.reshape(neg_anchors_ixs, [batch_size, -1])          # (batch_sz, ?)
    gt_boxes_pos = np.reshape(gt_boxes_pos, [batch_size, -1, gt_boxes_pos.shape[1]])  # (batch_sz, ?, 7)
    return pos_anchors_ixs, neg_anchors_ixs, gt_boxes_pos


def distance_calculator_np(anchors, gt_boxes):
    """
    Calculates Euclidean distances between anchors and gt box centers. Returns the distances as a matrix.
    :param anchors: (batch_size, n_anchors, 2), anchors with two parameters x and y
    :param gt_boxes: (batch_size, n_boxes, 5), 2D bev gt boxes.
    :return dist_mat: (batch_size, n_anchors, n_boxes) matrix of distance between anchors and gt boxes.

             _______ gt boxes ______
            |   .   .   .   .   .   .
    anchors |   .   .   .   .   .   .
            |   .   . Matrix    .   .
            |   .   .   .   .   .   .
    """
    n_anchors = anchors.shape[1]
    n_boxes = gt_boxes.shape[1]

    anchors = np.tile(anchors[:, :, None, :], [1, 1, n_boxes, 1])           # (batch_sz, n_anchors, n_boxes, 2)

    box_center = gt_boxes[..., 1:3]                                         # (batch_sz, n_boxes, 2)
    box_center = np.tile(box_center[:, None, :, :], [1, n_anchors, 1, 1])   # (batch_sz, n_anchors, n_boxes, 2)

    dist_mat = np.linalg.norm(anchors - box_center, axis=-1)                # (batch_sz, n_anchors, n_boxes)
    return dist_mat