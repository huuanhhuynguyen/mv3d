import tensorflow as tf
from tensorflow.image import non_max_suppression
from tensorflow.keras.layers import Layer


class NonMaxSuppression(Layer):

    def __init__(self, n_boxes_out=300, iou=0.5):
        """
        :param n_boxes_out: int, number of boxes after nms
        :param iou: float, iou threshold
        """
        super().__init__(name='nms')
        self.n_boxes_out = n_boxes_out
        self.iou = iou

    def call(self, inputs, **kwargs):
        """
        :param inputs: boxes (batch_size, n_boxes, 4), scores (batch_size, n_boxes)
        :return: indices of n_boxes_out boxes after NMS (batch_size, top_k)
        """
        boxes, scores = inputs

        n_boxes_out_tf = tf.expand_dims(tf.convert_to_tensor(self.n_boxes_out), 0)
        iou_tf = tf.expand_dims(tf.convert_to_tensor(self.iou), 0)

        ix = tf.map_fn(self.nms, [boxes, scores, n_boxes_out_tf, iou_tf], dtype=tf.int32)

        return ix

    def nms(self, inputs):
        box, score, n_boxes_out, iou = inputs
        box_ix = non_max_suppression(box, score, n_boxes_out, iou)

        return box_ix
