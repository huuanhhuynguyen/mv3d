import numpy as np

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv2D, MaxPooling2D


class VGG16(Model):

    """
    Source:
    VGG16: https://arxiv.org/abs/1409.1556
    VGG16 in mv3d only stops after 3 max pooling and n_channels are reduced to a half of the original network.
    """

    def __init__(self):
        super().__init__(name='backbone_vgg16')
        self.conv1 = Conv2D(filters=32, kernel_size=(3, 3), strides=1, padding='same', activation='relu',
                            kernel_initializer='he_uniform', name='conv1')
        self.conv2 = Conv2D(filters=32, kernel_size=(3, 3), strides=1, padding='same', activation='relu',
                            kernel_initializer='he_uniform', name='conv2')
        self.max_pool1 = MaxPooling2D(pool_size=(2, 2), strides=2, padding='same', name='max_pool1')

        self.conv3 = Conv2D(filters=64, kernel_size=(3, 3), strides=1, padding='same', activation='relu',
                            kernel_initializer='he_uniform', name='conv3')
        self.conv4 = Conv2D(filters=64, kernel_size=(3, 3), strides=1, padding='same', activation='relu',
                            kernel_initializer='he_uniform', name='conv4')
        self.max_pool2 = MaxPooling2D(pool_size=(2, 2), strides=2, padding='same', name='max_pool2')

        self.conv5 = Conv2D(filters=128, kernel_size=(3, 3), strides=1, padding='same', activation='relu',
                            kernel_initializer='he_uniform', name='conv5')
        self.conv6 = Conv2D(filters=128, kernel_size=(3, 3), strides=1, padding='same', activation='relu',
                            kernel_initializer='he_uniform', name='conv6')
        self.conv7 = Conv2D(filters=128, kernel_size=(3, 3), strides=1, padding='same', activation='relu',
                            kernel_initializer='he_uniform', name='conv7')
        self.max_pool3 = MaxPooling2D(pool_size=(2, 2), strides=2, padding='same', name='max_pool3')

    def call(self, x, **kwargs):
        x = self.conv2(self.conv1(x))
        x = self.max_pool1(x)
        x = self.conv4(self.conv3(x))
        x = self.max_pool2(x)
        x = self.conv7(self.conv6(self.conv5(x)))
        x = self.max_pool3(x)
        return x

    def compute_output_shape(self, input_shape):
        H, W, _ = tf.TensorShape(input_shape).as_list()
        output_shape = (H/8, W/8, 128)
        return tf.TensorShape(output_shape)


if __name__ == '__main__':
    batch_size = 1
    input_tensor = np.random.rand(batch_size, 704, 800, 42)
    backbone = VGG16()
    output_tensor = backbone(tf.convert_to_tensor(input_tensor))

    print(output_tensor.shape)
    assert output_tensor.shape == (batch_size, 704/8, 800/8, 128)

    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        output = sess.run(output_tensor)