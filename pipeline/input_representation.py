import numpy as np
from math import log, pi
from collections import defaultdict
from functools import partial

from pipeline.utils.coordinates import DX, DY, DZ, X_MAX, X_MIN, Y_MAX, Y_MIN, Z_MIN, Z_MAX


class BEVRepresentation:

    def __init__(self, x_range=(X_MIN, X_MAX), y_range=(Y_MIN, Y_MAX), z_range=(Z_MIN, Z_MAX)):
        self.x_min, self.x_max = x_range
        self.y_min, self.y_max = y_range
        self.z_min, self.z_max = z_range

    def encode(self, lidar_data, dx=DX, dy=DY, dz=DZ):
        lidar_data = np.array(lidar_data)
        encode_fn = partial(self._encode, dx=dx, dy=dy, dz=dz)

        if len(lidar_data.shape) == 2:
            return encode_fn(lidar_data)

        elif len(lidar_data.shape) == 3:
            bev_batch = np.array(tuple(map(encode_fn, lidar_data)))
            return bev_batch

        else:
            raise ValueError("lidar_data must have a rank of 2 (batch_size=1)"
                             "or 3 (batch_size>1) but its rank is "
                             "{}.".format(len(lidar_data.shape)))

    def _encode(self, lidar_data, dx=DX, dy=DY, dz=DZ):
        """
        Encodes one frame.
        N = num of points, M = number of slices in z-direction
        X, Y = number of intervals in x,y-direction
        :param lidar_data: (N, 4) whose row is a data point (x, y, z, reflectance)
        :return: bev_representation (X, Y, M+2), where 2 for intensity and density
        Time complexity O(N + X*Y)
        """
        X = int((self.x_max - self.x_min) / dx)
        Y = int((self.y_max - self.y_min) / dy)
        M = int((self.z_max - self.z_min) / dz)
        bev = np.full(shape=(X, Y, M+2), fill_value=float('-inf'))

        # discretizes x, y, z into (cell) indicies
        x, y, z, refl = np.split(np.array(lidar_data), 4, axis=-1)
        i_x = ((x - self.x_min) / dx).astype(np.int)
        i_y = ((y - self.y_min) / dy).astype(np.int)
        i_z = ((z - self.z_min) / dz).astype(np.int)

        # flip bev horizontally and vertically to match with our convention
        i_x = X - i_x
        i_y = Y - i_y

        # keeps only indicies within the given range
        data_with_ixs = np.stack([i_x, i_y, i_z, z, refl], axis=-1).squeeze(1)
        valid_ix = np.logical_and(0 <= i_x, i_x < X)
        valid_iy = np.logical_and(0 <= i_y, i_y < Y)
        valid_iz = np.logical_and(0 <= i_z, i_z < M)
        valid_data_point = np.logical_and(valid_ix, valid_iy)
        valid_data_point = np.logical_and(valid_data_point, valid_iz).squeeze(1)
        data_with_ixs = data_with_ixs[valid_data_point]

        n_points_pillar = defaultdict(int)  # counts n_points in each pillar
        z_max = {}  # stores max height in each pillar
        for (ix, iy, iz, height, intensity) in data_with_ixs:
            ix, iy, iz = int(ix), int(iy), int(iz)

            # Calculate height of each cell
            if height > bev[ix, iy, iz]:
                bev[ix, iy, iz] = height
                # calculate intensity of each pillar
                if (ix, iy) not in z_max or height > z_max[(ix, iy)]:
                    z_max[(ix, iy)] = height
                    bev[ix, iy, M] = intensity

            n_points_pillar[(ix, iy)] += 1

        # Calculate density
        for (ix, iy), n_points in n_points_pillar.items():
            bev[ix, iy, M+1] = min(1.0, log(n_points+1) / log(64))

        bev[bev == float('-inf')] = 0
        return bev


class FVRepresentation:

    # According to mv3d paper - 3.4 Input Representation, we want
    # a FV representation map of 64 x 512
    v_min, v_max = -24.8 / 180 * pi, 2.0 / 180 * pi / 2
    h_min, h_max = -pi / 4, pi / 4
    V, H = 64, 512
    dv = (v_max-v_min) / V  # angular vertical resolution
    dh = (h_max-h_min) / H  # angular horizontal resolution

    def encode(self, lidar_data):
        """
        Encodes one frame.
        N = num of points, M = number of slices
        H, V = number of intervals in horiz-vertical direction
        :param lidar_data: (N, 4) whose row is a data point (x, y, z, reflectance)
                           (batch_size, N, 4)
        :return: fv_representation (H, V, 3), where 3 for height, distance and intensity
        Time complexity O(N)
        """
        lidar_data = np.array(lidar_data)

        if len(lidar_data.shape) == 2:
            fv = np.zeros(shape=(self.V, self.H, 3))
        elif len(lidar_data.shape) == 3:
            batch_size = lidar_data.shape[0]
            fv = np.zeros(shape=(batch_size, self.V, self.H, 3))
        else:
            raise ValueError("lidar_data must have a rank of 2 (batch_size=1)"
                             "or 3 (batch_size>1) but its rank is "
                             "{}.".format(len(lidar_data.shape)))

        x, y, z, refl = np.split(lidar_data, 4, axis=1)

        # Calculate polar coord (h, v)
        v = np.arctan2(z, np.sqrt(x*x + y*y))
        h = np.arctan2(y, x)

        # Discretize into array indices
        i_v = ((v - self.v_min) / self.dv).astype(np.int)
        i_h = ((self.h_max - h) / self.dh).astype(np.int)

        # filter out of range points
        in_bound = (0 <= i_v) & (i_v < self.V) & (0 <= i_h) & (i_h < self.H)
        i_v = i_v[..., in_bound]
        i_h = i_h[..., in_bound]
        z = z[..., in_bound]
        x = x[..., in_bound]
        y = y[..., in_bound]
        refl = refl[..., in_bound]

        # Set values into array
        height, dist, intensity = 0, 1, 2
        fv[..., self.V-i_v-1, i_h, height] = z
        fv[..., self.V-i_v-1, i_h, dist] = np.sqrt(x*x + y*y + z*z)
        fv[..., self.V-i_v-1, i_h, intensity] = refl

        return fv