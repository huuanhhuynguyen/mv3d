from third_party.roipooling_op import *
from tensorflow.keras.layers import Layer


class ROIPool(Layer):

    def __init__(self, height, width, stride):
        super().__init__(name='roi_pooling')
        self.height = height
        self.width = width
        self.downsampling = 1.0/stride


    def call(self, inputs, **kwargs):
        """
        Call function of ROI Pooling operation
            :param inputs: feature_maps (batch_size, fm_height, fm_width, fm_depth), rois (batch_id, y1, x1, y2, x2)
            ------> X
        |
        |
        v
        Y
        :return: pooled rois (batch_size, height, width, feature_map_depth)
        """
        feature_maps, rois = inputs

        return roi_pool(feature_maps, rois, self.height, self.width, self.downsampling)