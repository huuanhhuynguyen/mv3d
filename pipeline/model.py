import tensorflow as tf
from tensorflow.losses import softmax_cross_entropy, absolute_difference, huber_loss
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv2DTranspose

from pipeline.backbone import VGG16
from pipeline.rpn import RPN, AnchorGenerator, Matcher, BoxCoder


class MV3D(Model):

    def __init__(self, mode='train'):
        super().__init__(name='mv3d')
        self.mode = mode

        self.backbone = VGG16()  # M = 5 => M+2 = 7
        self.rpn = RPN(B=8)
        self.rpn_deconv = Conv2DTranspose(filters=128, kernel_size=(3, 3), strides=2, padding='same',
                                                    activation='relu', kernel_initializer='he_uniform')
        self.metrics_names = ['cls_metric', 'reg_metric']

    def call(self, inputs, **kwargs):
        if self.mode == 'train':
            bev, gt_boxes = inputs
            obj_score, reg = self.call_train(bev, gt_boxes)
            return obj_score, reg

        if self.mode == 'inference':
            bev = inputs[0]
            obj_score, reg = self.call_inference(bev)
            return obj_score, reg

    def call_train(self, bev, gt_boxes_3d):
        feature_maps = self.backbone(bev)
        feature_maps = self.rpn_deconv(feature_maps)
        obj_score, reg = self.rpn(feature_maps)

        # generate anchors
        anchor_gen = AnchorGenerator()
        anchors = anchor_gen(feature_maps)

        # match anchor with gt boxes
        matcher = Matcher()
        pos_anchors_ixs, neg_anchors_ixs, gt_boxes_pos = matcher([anchors, gt_boxes_3d])

        pos_anchors = tf.map_fn(gather, [anchors, pos_anchors_ixs], dtype=tf.float32)
        gt_boxes_pos = BoxCoder.encode(gt_boxes_pos, pos_anchors)

        with tf.name_scope('cls_loss'):
            cls_loss = self._cal_cls_loss(softmax_cross_entropy, obj_score, pos_anchors_ixs, neg_anchors_ixs)
        with tf.name_scope('reg_loss'):
            reg_loss = self._cal_reg_loss(huber_loss, reg, pos_anchors_ixs, gt_boxes_pos)

        self.add_loss(reg_loss)
        self.add_loss(cls_loss)

        return obj_score, reg

    def call_inference(self, bev):
        feature_maps = self.backbone(bev)
        logits, reg = self.rpn(feature_maps)
        obj_score = softmax(logits)
        return obj_score, reg

    def _cal_reg_loss(self, loss_fn, reg, pos_anchors_ixs, gt_boxes_pos):
        """
        :param loss_fn: callable, regression loss function
        :param reg: (batch_size, fm_height, fm_width, K*n_box_coding_params), regression prediction of the anchors
        Where n_anchors = fm_height * fm_width * K
        :param pos_anchors_ixs: (batch_size, n_pos_anchors,), indices of positive anchors
        :param gt_boxes_pos: (batch_size, n_pos_anchors,), gt boxes corresponding to the positive anchors
        :return: (batch_size, n_pos_anchors), regression loss tensor
        """
        batch_size = tf.shape(reg)[0]
        K = self.rpn.num_anchor_per_grid
        reg = tf.reshape(reg, [batch_size, -1, 8*K])

        predictions = tf.map_fn(gather, [reg, pos_anchors_ixs], dtype=tf.float32)
        return loss_fn(gt_boxes_pos, predictions)

    def _cal_cls_loss(self, loss_fn, obj_score, pos_anchors_ixs, neg_anchors_ixs):
        """
        :param loss_fn: callable, classification loss function
        :param obj_score: (batch_size, fm_height, fm_width, K*2), classification prediction of the anchors
        :param pos_anchors_ixs: (batch_size, n_pos_anchors,), indices of positive anchors
        :param neg_anchors_ixs: (batch_size, n_neg_anchors,), indices of negative anchors
        :return: (batch_size, n_pos_anchors + n_neg_anchors), classification loss tensor
        """
        batch_size = tf.shape(obj_score)[0]
        K = self.rpn.num_anchor_per_grid
        obj_score = tf.reshape(obj_score, [batch_size, -1, 2*K])

        # create pos targets  #TODO use keras utils categorical?
        ones = tf.ones_like(pos_anchors_ixs, dtype=tf.float32)
        zeros = tf.zeros_like(pos_anchors_ixs, dtype=tf.float32)
        pos_targets = tf.stack((ones, zeros), axis=-1)

        # create neg targets
        ones = tf.ones_like(neg_anchors_ixs, dtype=tf.float32)
        zeros = tf.zeros_like(neg_anchors_ixs, dtype=tf.float32)
        neg_targets = tf.stack((zeros, ones), axis=-1)

        targets = tf.concat([pos_targets, neg_targets], axis=1)

        # find prediction
        ixs = tf.concat([pos_anchors_ixs, neg_anchors_ixs], axis=1)
        predictions = tf.map_fn(gather, [obj_score, ixs], dtype=tf.float32)

        return loss_fn(targets, predictions)


def gather(inputs):
    params, ixs = inputs
    return tf.gather(params, ixs)


if __name__ == '__main__':
    from pipeline.input_representation import BEVRepresentation
    from pipeline.utils.read import read_calib_matrices, read_gt_obbs, read_velo

    import numpy as np

    model = MV3D(mode='train')

    # get bev
    frame_no = 9
    frame = read_velo(frame_no)
    bev_rep = BEVRepresentation()
    bev = bev_rep.encode(frame)

    # get gt boxes
    M = read_calib_matrices(frame_no)
    boxes3d = read_gt_obbs(M['Tr_velo_to_cam'], frame_no, categories=["Car"])  # xyz, thetaY in camera coordinates
    boxes3d = boxes3d.astype(np.float32)

    # tensor
    bev_batch = bev[None, ...]
    boxes3d_batch = boxes3d[None, ...]
    bev_batch = tf.convert_to_tensor(bev_batch)
    boxes3d_batch = tf.convert_to_tensor(boxes3d_batch)

    output = model([bev_batch, boxes3d_batch])

    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(output)

    #TODO write tests to train with 2 batches, each batch with size=4
