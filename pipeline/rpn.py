import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Conv2D, Layer

from pipeline.utils.coordinates import X_MIN, X_MAX, X, Y_MIN, Y_MAX, Y, to_bev_boxes, to_bev_boxes_tf


class RPN(Model):
    def __init__(self, B, K=1):
        """
        :param B: int, number of box coding parameters.
        :param K: int, number of anchors per grid location.
        """
        super().__init__(name='rpn')

        self.conv1 = Conv2D(64, kernel_size=3, strides=1, padding='same',
                            activation='relu', name='head')
        self.cls = Conv2D(2 * K, kernel_size=1, activation='softmax', name='cls')
        self.reg = Conv2D(B * K,  kernel_size=1, activation='linear', name='reg')
        self._K = K

    def call(self, x, **kwargs):
        x = self.conv1(x)
        logits = self.cls(x)
        reg = self.reg(x)
        return logits, reg

    def compute_output_shape(self, input_shape):
        H, W, _ = tf.TensorShape(input_shape).as_list()
        cls_shape = (H, W, self.cls.filters)
        reg_shape = (H, W, self.reg.filters)
        return tf.TensorShape(cls_shape), tf.TensorShape(reg_shape)

    @property
    def num_anchor_per_grid(self):
        return self._K


class AnchorGenerator(Layer):
    """
    Generate a grid of anchors. At every grid location, there are K anchors. K equals 4 by default, which corresponds to
    four different theta values.
    Each anchor has 3 parameters x, y, theta.
    """
    def __init__(self):
        super().__init__(name='anchor_generator')

    def call(self, feature_map, **kwargs):
        _, H, W, _ = feature_map.shape
        batch_size = tf.shape(feature_map)[0]
        assert H == X/4 and W == Y/4, "The feature_map must have shape ({}, {}, _)".format(int(X/4), int(Y/4))

        # generate possible anchor x, y values
        x_anchor = tf.linspace(X_MIN, X_MAX, H)
        y_anchor = tf.linspace(Y_MIN, Y_MAX, W)
        z_l_w_h_anchor = tf.constant([[-1.0, 3.9, 1.6, 1.56]])
        n_base_anchors = tf.shape(z_l_w_h_anchor)[0]

        # mesh grid
        x_anchor, y_anchor = tf.meshgrid(x_anchor, y_anchor)

        # flatten and tile
        x_anchor = tf.reshape(x_anchor, [-1])
        x_anchor_tiled = tf.tile(tf.expand_dims(x_anchor, -1), [1, n_base_anchors])
        x_anchor_tiled = tf.reshape(x_anchor_tiled, [-1, 1])

        y_anchor = tf.reshape(y_anchor, [-1])
        y_anchor_tiled = tf.tile(tf.expand_dims(y_anchor, -1), [1, n_base_anchors])
        y_anchor_tiled = tf.reshape(y_anchor_tiled, [-1, 1])

        theta_anchor = tf.zeros_like(x_anchor_tiled, dtype=tf.float32)

        n_anchor_points = tf.shape(x_anchor)[0]
        z_l_w_h_anchor = tf.tile(z_l_w_h_anchor, [n_anchor_points, 1])

        # This is done to expand the logic for more anchors
        anchors = tf.concat([theta_anchor, x_anchor_tiled, y_anchor_tiled, z_l_w_h_anchor], axis=-1)
        anchors = tf.expand_dims(anchors, 0)
        anchors = tf.tile(anchors, [batch_size, 1, 1])

        return anchors


class Matcher(Layer):
    """
    Classify anchors as pos, neg, ignored and assign gt boxes to pos anchors.
        - Pos anchors are anchors that have distance to gt box center less than 1.5 meter.
        - Neg anchors are anchors that have distance to gt box center greater than or equal 1.5 meter.
        - Ignored anchors are anchors that are neither pos or neg.
    """
    def __init__(self, radius=2.0):
        self.dist_calc = DistanceCalculator()
        super().__init__(name='matcher')
        self.radius = radius

    def call(self, inputs, **kwargs):
        """
        :param inputs: tuple, that contains:
                    - anchors: (batch_size, n_anchors, 7)
                    - gt_boxes_3d: (batch_size, ?, 7) where ? is n_boxes.
        :return pos_anchors: (batch_size, n_pos_anchors, 2) positive anchors
                neg_anchors: (batch_size, n_neg_anchors, 2) negative anchors
                gt_boxes_pos: (batch_size, n_pos_anchors, 7) gt boxes where each box corresponds to each positive anchor

        Note that n_anchors = n_pos_anchors + n_neg_anchors + n_ignored_anchors
        """
        anchors, gt_boxes_3d = inputs
        batch_size = tf.shape(anchors)[0]
        n_anchors = tf.shape(anchors)[1]
        n_boxes = tf.shape(gt_boxes_3d)[1]

        # 3d boxes to 2d
        gt_boxes = to_bev_boxes_tf(gt_boxes_3d)

        # Find distance matrix
        dists = self.dist_calc([anchors, gt_boxes])        # (batch_sz, n_anchors, n_boxes)

        # For each anchor, only keep the distance to the closest box
        dists_min = tf.reduce_min(dists, axis=-1)                   # (batch_sz, n_anchors,)
        dists_min = tf.expand_dims(dists_min, 2)
        dists_min = tf.tile(dists_min, [1, 1, n_boxes])             # (batch_sz, n_anchors, n_boxes)
        dists = tf.where(tf.greater(dists, dists_min), dists * 0 + 1000, dists)  # very large

        # Find distance mask
        mask_dist = tf.less(dists, self.radius)

        # Tile anchors and gt_boxes
        gt_boxes_3d = tf.expand_dims(gt_boxes_3d, 1)
        gt_boxes_3d = tf.tile(gt_boxes_3d, [1, n_anchors, 1, 1])    # (batch_sz, n_anchors, n_boxes, 5)

        # Get positive, negative anchors and gt_boxes_pos
        mask_anchor = tf.reduce_any(mask_dist, axis=-1)                               # (batch_sz, n_anchors)
        anchors_ixs = tf.tile(tf.expand_dims(tf.range(n_anchors), 0), [batch_size, 1])  # (batch_sz, n_anchors)
        pos_anchors_ixs = tf.boolean_mask(anchors_ixs, mask_anchor)                     # (batch_sz x ?,)
        neg_anchors_ixs = tf.boolean_mask(anchors_ixs, tf.logical_not(mask_anchor))     # (batch_sz x ?,)
        gt_boxes_pos = tf.boolean_mask(gt_boxes_3d, mask_dist)                        # (batch_sz x ?, 7)

        # Reshape
        pos_anchors_ixs = tf.reshape(pos_anchors_ixs, [batch_size, -1])                 # (batch_sz, ?)
        neg_anchors_ixs = tf.reshape(neg_anchors_ixs, [batch_size, -1])                 # (batch_sz, ?)
        gt_boxes_pos = tf.reshape(gt_boxes_pos, [batch_size, -1, tf.shape(gt_boxes_pos)[1]])  # (batch_sz, ?, 7)
        return pos_anchors_ixs, neg_anchors_ixs, gt_boxes_pos


class DistanceCalculator(Layer):
    """
    Calculates Euclidean distances between anchors and gt box centers. Returns the distances as a matrix.
    """
    def ___init__(self):
        super().__init__(name='dist_calc')

    def call(self, inputs, **kwargs):
        """
        :param inputs:
                - anchors: (batch_size, n_anchors, 7), anchors with 7 parameters (theta, x, y, z, l, w, h)
                - gt_boxes: (batch_size, n_boxes, 5), 2D bev gt boxes.
        :return dists: (batch_size, n_anchors, n_boxes) matrix of distance between anchors and gt boxes.

                 _______ gt boxes ______
                |   .   .   .   .   .   .
        anchors |   .   .   .   .   .   .
                |   .   . Matrix    .   .
                |   .   .   .   .   .   .
        """
        anchors, gt_boxes = inputs
        n_anchors = tf.shape(anchors)[1]
        n_boxes = tf.shape(gt_boxes)[1]

        anchors_xy = anchors[..., 1:3]
        anchors_xy = tf.expand_dims(anchors_xy, 2)
        anchors_xy = tf.tile(anchors_xy, [1, 1, n_boxes, 1])    # (batch_sz, n_anchors, n_boxes, 2)

        box_center = gt_boxes[..., 1:3]                         # (batch_sz, n_boxes, 2)
        box_center = tf.expand_dims(box_center, 1)
        box_center = tf.tile(box_center, [1, n_anchors, 1, 1])  # (batch_sz, n_anchors, n_boxes, 2)

        dists = tf.norm(anchors_xy - box_center, axis=-1)       # (batch_sz, n_anchors, n_boxes)
        return dists


class BoxCoder:
    """
    Box coding method is based on:
    Source: https://arxiv.org/pdf/1506.01497.pdf
    Mean length, width and height are taken from mv3d paper.
    """
    @staticmethod
    def encode(boxes3d, anchors):
        """
        Encodes 3D bounding boxes without orientation
        :param boxes3d: (batch_size, n_boxes, 7), columns represent (theta, x, y, z, l, w, h)
        :param anchors: (batch_size, n_anchors, 7) anchors (theta_a, xa, ya, za, la, wa, ha)
        :return codes: (batch_size, n_boxes, 6)
        """
        theta, x, y, z, l, w, h = tf.split(boxes3d, 7, axis=-1)
        _, xa, ya, za, la, wa, ha = tf.split(anchors, 7, axis=-1)

        tx = (x-xa) / la
        ty = (y-ya) / wa
        tz = (z-za) / ha

        tl = tf.log(l/la)
        tw = tf.log(w/wa)
        th = tf.log(h/ha)

        codes = tf.concat([tx, ty, tz, tl, tw, th], axis=-1)
        return codes

    @staticmethod
    def decode(codes, anchors):
        """
        Decodes back into 3D oriented bounding boxes.
        :param codes: (batch_size, n_boxes, 6)
        :param anchors: (batch_size, n_anchors, 7) anchors
        :return boxes3d: (batch_size, n_boxes, 7)
        """
        tx, ty, tz, tl, tw, th = tf.split(codes, 6, axis=-1)
        theta_a, xa, ya, za, la, wa, ha = tf.split(anchors, 7, axis=-1)

        x = (tx * la) + xa
        y = (ty * wa) + ya
        z = (tz * ha) + za

        l = tf.exp(tl) * la
        w = tf.exp(tw) * wa
        h = tf.exp(th) * ha

        boxes3d = tf.concat([theta_a, x, y, z, l, w, h], axis=-1) # theta_a not regressed
        return boxes3d
