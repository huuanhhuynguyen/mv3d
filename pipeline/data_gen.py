from os.path import join, dirname
import glob
from math import ceil
from tensorflow.keras.utils import Sequence

from pipeline.input_representation import BEVRepresentation
from pipeline.utils.read import read_calib_matrices, read_gt_obbs, read_velo


class DataGenerator(Sequence):

    def __init__(self, dir=None, batch_size=1):
        if dir is None:
            dir = join(dirname(dirname(__file__)), 'kitti_mini_dataset')
        self.dir = dir
        self.batch_size = batch_size
        self.frame_no = 0
        self.num_frames = get_num_frames(dir)
        self._bev_rep = BEVRepresentation()

    def __len__(self):
        return ceil(self.num_frames / self.batch_size)

    def __getitem__(self, ix):
        #TODO read a batch, the following is hard-coded for batch_size = 1
        frame_no = ix
        frame = read_velo(frame_no)
        bev = self._bev_rep.encode(frame)

        M = read_calib_matrices(frame_no)
        boxes3d = read_gt_obbs(M['Tr_velo_to_cam'], frame_no)

        return [bev[None, ...], boxes3d[None, ...]], None


def get_num_frames(dir):
    dir = join(dir, 'lidar')
    bin_files = glob.glob(dir + '/*.bin')
    return len(bin_files)
