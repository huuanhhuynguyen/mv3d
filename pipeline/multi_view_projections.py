from pipeline.utils.coordinates import xy_to_bev_array_index, xy_to_bev_array_index_tf
import numpy as np
import tensorflow as tf
from math import pi
from functools import reduce


# Front view dimension parameters
v_min, v_max = -24.8 / 180 * pi, 2.0 / 180 * pi / 2
h_min, h_max = -pi / 4, pi / 4  # lidar points within FOV of camera
V, H = 64, 512
dv = (v_max - v_min) / V  # angular vertical resolution
dh = (h_max - h_min) / H  # angular horizontal resolution


# Camera image dimensions
image_hor = 1242
image_ver = 375


def box3d_to_bev(boxes3d):
    """
    Projects 3D boxes from LiDAR co-ordinate system to Top View co-ordinate system.
    :param boxes3d: (n_boxes, 8, 3)
    :return bev_boxes: (n_boxes, 4, 2) four corners of bev box in order (x1, y1), (x2, y1), (x2, y2), (x1, y2)
    """
    is_reshape = boxes3d.shape == (8, 3)  # support for single box3d
    if is_reshape:
        boxes3d = boxes3d.reshape(1, 8, 3)

    x, y = np.split(boxes3d[:, :4, :2], 2, axis=2)  # (n_boxes, 4, 1)
    u, v = xy_to_bev_array_index(x, y)  # (n_boxes, 4, 1)
    u = np.clip(u, 0, 704)
    v = np.clip(v, 0, 800)
    bev_boxes = np.concatenate([u, v], axis=2)  # (n_boxes, 4, 2)

    if is_reshape:
        bev_boxes = bev_boxes.squeeze(0)

    return bev_boxes


def box3d_to_fv(boxes3d):
    """
    Projects 3D boxes from LiDAR frame to the front view.
    :param  boxes3d: (n_boxes, 8, 3)
    :return fv_boxes: (n_boxes, 4, 2) four corners of front view box in order (x1, y1), (x2, y1), (x2, y2), (x1, y2)
    """

    is_reshape = boxes3d.shape == (8, 3)  # support for single box3d

    if is_reshape:
        boxes3d = boxes3d.reshape(1, 8, 3)

    x, y, z = np.split(boxes3d, 3, axis=-1)

    hor, ver = _lidar_to_fv_coords(x, y, z)

    # clip between 64x512 image boundary
    hor_min = np.clip(np.min(hor, axis=1), 0, H)
    hor_max = np.clip(np.max(hor, axis=1), 0, H)
    ver_min = np.clip(np.min(ver, axis=1), 0, V)
    ver_max = np.clip(np.max(ver, axis=1), 0, V)

    row1 = np.concatenate([hor_min, ver_min], axis=1)
    row2 = np.concatenate([hor_max, ver_min], axis=1)
    row3 = np.concatenate([hor_max, ver_max], axis=1)
    row4 = np.concatenate([hor_min, ver_max], axis=1)

    fv_boxes = np.stack([row1, row2, row3, row4], axis=1)

    return fv_boxes


def _lidar_to_fv_coords(x, y, z):
    """
    Convert (x,y,z) from LiDAR co-ordinate to (horz, vert) in the front view.
    :param x, y, z: (n_boxes, ...)
    :return (horz, vert): (n_boxes, ...)
    """
    v = np.arctan2(z, np.sqrt(x * x + y * y))
    h = np.arctan2(y, x)

    i_h = ((h_max - h) / dh).astype(np.int)
    i_v = ((v - v_min) / dv).astype(np.int)

    horz = i_h
    vert = V - i_v - 1

    return horz, vert


def box3d_to_camera2d(boxes3d, Tr_velo_cam, projection_mat, rect_mat):
    """
    3D box corners (ISO) to 2D box corners in camera image view
    :param boxes3d: (n_boxes, 8, 3)
    :param Tr_velo_cam: (4, 4), the extrinsic matrix to transform a 3D point (x,y,z) from LiDAR co-ordinate to a 3D point
    (x,y,z) in camera co-ordinate.
    :param projection_mat: (3, 4), the intrinsic matrix to transform a 3D point (x,y,z) from camera co-ordinate to a 2D
     point (u,v) in camera image co-ordinate.
    :param rect_mat: (4, 4), rectifying rotation matrix.
    :return camera_roi_2d: (n_boxes, 4, 2) four corners of bounding box in order (x1, y1), (x2, y1), (x2, y2), (x1, y2)
    """
    is_reshape = boxes3d.shape == (8, 3)  # support for single box3d

    if is_reshape:
        boxes3d = boxes3d.reshape(1, 8, 3)

    camera_roi_3d = box3d_to_camera3d(boxes3d, Tr_velo_cam, projection_mat, rect_mat)

    minx = np.min(camera_roi_3d[..., 0], axis=1)
    maxx = np.max(camera_roi_3d[..., 0], axis=1)
    miny = np.min(camera_roi_3d[..., 1], axis=1)
    maxy = np.max(camera_roi_3d[..., 1], axis=1)

    minx = np.clip(minx, 0, image_hor).reshape(-1, 1)
    miny = np.clip(miny, 0, image_ver).reshape(-1, 1)
    maxx = np.clip(maxx, 0, image_hor).reshape(-1, 1)
    maxy = np.clip(maxy, 0, image_ver).reshape(-1, 1)

    row1 = np.concatenate([minx, miny], axis=1)
    row2 = np.concatenate([maxx, miny], axis=1)
    row3 = np.concatenate([maxx, maxy], axis=1)
    row4 = np.concatenate([minx, maxy], axis=1)
    camera_roi_2d = np.stack([row1, row2, row3, row4], axis=1)

    return camera_roi_2d


def box3d_to_camera3d(boxes3d, Tr_velo_cam, projection_mat, rect_mat):
    """
    Transforms 3D box corners (ISO) to 3D box corners in camera image view.
    :param boxes3d: (n_boxes, 8, 3)
    :param Tr_velo_cam: (4, 4), the extrinsic matrix to transform a 3D point (x,y,z) from LiDAR co-ordinate to a 3D point
    (x,y,z) in camera co-ordinate.
    :param projection_mat: (3, 4), the intrinsic matrix to transform a 3D point (x,y,z) from camera co-ordinate to a 2D
     point (u,v) in camera image view.
    :param rect_mat: (4, 4), rectifying rotation matrix.
    :return uv: (n_boxes, 8, 2) 8 corners in camera image view
    """
    is_reshape = boxes3d.shape == (8, 3)  # support for single box3d

    if is_reshape:
        boxes3d = boxes3d.reshape(1, 8, 3)

    n_boxes = len(boxes3d)
    ones = np.ones((n_boxes, 8, 1))
    corners_in_lidar = np.concatenate((boxes3d, ones), axis=2)  # (n_boxes, 8, 4)
    corners_in_image = (reduce(np.dot, (projection_mat, rect_mat, Tr_velo_cam, corners_in_lidar[..., None]))).squeeze(3)
    corners_in_image = np.transpose(corners_in_image, [1, 2, 0])

    z = corners_in_image[:, :, 2][..., None]  # (n_boxes, 8, 3)
    corners_in_image = (corners_in_image / z).astype(np.int32)
    uv = corners_in_image[:, :, :2]  # (n_boxes, 8, 2)
    uv[:, :, 0] = np.clip(uv[:, :, 0], 0, image_hor)
    uv[:, :, 1] = np.clip(uv[:, :, 1], 0, image_ver)

    if is_reshape:
        uv = uv.squeeze(0)

    return uv


def box3d_to_bev_tf(boxes3d):
    """
    Projects 3D boxes from LiDAR co-ordinate system to Top View co-ordinate system - Tensorflow version.
    :param boxes3d: (batch_size, n_boxes, 8, 3)
    :return bev_boxes: (batch_size, n_boxes, 4, 2) four corners of bev box in order (x1, y1), (x2, y1), (x2, y2),
    (x1, y2)
    """
    x, y = tf.split(boxes3d[:, :, :4, :2], 2, axis=3)  # (batch_size, n_boxes, 4, 1)
    u, v = xy_to_bev_array_index_tf(x, y)  # (batch_size, n_boxes, 4, 1)

    u = tf.clip_by_value(u, 0, 704)
    v = tf.clip_by_value(v, 0, 800)

    bev_boxes = tf.concat([u, v], axis=3)  # (batch_size, n_boxes, 4, 1)

    return bev_boxes


def box3d_to_fv_tf(boxes3d):
    """
    Projects 3D boxes from LiDAR frame to the front view - Tensorflow version.
    :param  boxes3d: (batch_size, n_boxes, 8, 3)
    :return fv_boxes: (batch_size, n_boxes, 4, 2) four corners of front view box in order (x1, y1), (x2, y1), (x2, y2),
    (x1, y2)
    """
    x, y, z = tf.split(boxes3d, 3, axis=-1)

    hor, ver = _lidar_to_fv_coords_tf(x, y, z)

    # clip between 64x512 image boundary
    hor_min = tf.clip_by_value(tf.reduce_min(hor, axis=2), 0, H)
    hor_max = tf.clip_by_value(tf.reduce_max(hor, axis=2), 0, H)
    ver_min = tf.clip_by_value(tf.reduce_min(ver, axis=2), 0, V)
    ver_max = tf.clip_by_value(tf.reduce_max(ver, axis=2), 0, V)

    row1 = tf.concat([hor_min, ver_min], axis=2)
    row2 = tf.concat([hor_max, ver_min], axis=2)
    row3 = tf.concat([hor_max, ver_max], axis=2)
    row4 = tf.concat([hor_min, ver_max], axis=2)

    fv_boxes = tf.stack([row1, row2, row3, row4], axis=2)

    return fv_boxes


def _lidar_to_fv_coords_tf(x, y, z):
    """
    Convert (x,y,z) from LiDAR co-ordinate to (horz, vert) in the front view - Tensorflow version.
    :param x, y, z: (batch_size, n_boxes, ...)
    :return (horz, vert): (batch_size, n_boxes, ...)
    """
    v = tf.math.atan2(z, tf.sqrt(x * x + y * y))
    h = tf.math.atan2(y, x)

    i_h = tf.cast(((h_max - h) / dh), dtype=tf.int32)
    i_v = tf.cast(((v - v_min) / dv), dtype=tf.int32)

    horz = i_h
    vert = V - i_v - 1

    return horz, vert


def box3d_to_camera2d_tf(boxes3d, Tr_velo_cam, projection_mat, rect_mat):
    """
    3D box corners (ISO) to 2D box corners in camera image view - Tensorflow version
    :param boxes3d: (batch_size, n_boxes, 8, 3)
    :param Tr_velo_cam: (batch_size, 4, 4), the extrinsic matrix to transform a 3D point (x,y,z) from LiDAR
    co-ordinate to a 3D point (x,y,z) in camera co-ordinate.
    :param projection_mat: (batch_size, 3, 4), the intrinsic matrix to transform a 3D point (x,y,z) from camera
    co-ordinate to a 2D point (u,v) in camera image co-ordinate.
    :param rect_mat: (batch_size, 4, 4), rectifying rotation matrix.
    :return camera_roi_2d: (batch_size, n_boxes, 4, 2) four corners of bounding box in order (x1, y1), (x2, y1), (x2,
    y2), (x1, y2)
    """
    camera_roi_3d = box3d_to_camera3d_tf(boxes3d, Tr_velo_cam, projection_mat, rect_mat)

    minx = tf.reduce_min(camera_roi_3d[..., 0], axis=2)
    maxx = tf.reduce_max(camera_roi_3d[..., 0], axis=2)
    miny = tf.reduce_min(camera_roi_3d[..., 1], axis=2)
    maxy = tf.reduce_max(camera_roi_3d[..., 1], axis=2)

    minx = tf.expand_dims(tf.clip_by_value(minx, 0, image_hor), 2)
    miny = tf.expand_dims(tf.clip_by_value(miny, 0, image_ver), 2)
    maxx = tf.expand_dims(tf.clip_by_value(maxx, 0, image_hor), 2)
    maxy = tf.expand_dims(tf.clip_by_value(maxy, 0, image_ver), 2)

    row1 = tf.concat([minx, miny], axis=2)
    row2 = tf.concat([maxx, miny], axis=2)
    row3 = tf.concat([maxx, maxy], axis=2)
    row4 = tf.concat([minx, maxy], axis=2)
    camera_roi_2d = tf.stack([row1, row2, row3, row4], axis=2)

    return camera_roi_2d


def box3d_to_camera3d_tf(boxes3d, Tr_velo_cam, projection_mat, rect_mat):
    """
    Transforms 3D box corners (ISO) to 3D box corners in camera image view - Tensorflow version.
    :param boxes3d: (batch_size, n_boxes, 8, 3)
    :param Tr_velo_cam: (batch_size, 4, 4), the extrinsic matrix to transform a 3D point (x,y,z) from LiDAR
    co-ordinate to a 3D point (x,y,z) in camera co-ordinate.
    :param projection_mat: (batch_size, 3, 4), the intrinsic matrix to transform a 3D point (x,y,z) from camera
    co-ordinate to a 2D point (u,v) in camera image view.
    :param rect_mat: (batch_size, 4, 4), rectifying rotation matrix.
    :return uv: (batch_size, n_boxes, 8, 2) 8 corners in camera image view
    """
    batch_size = tf.shape(boxes3d)[0]
    n_boxes = tf.shape(boxes3d)[1]

    ones = tf.ones((batch_size, n_boxes, 8, 1), dtype=tf.float64)
    corners_in_lidar = tf.concat([boxes3d, ones], axis=3)  # (batch_size, n_boxes, 8, 4)
    corners_in_lidar = tf.transpose(corners_in_lidar, perm=[0, 1, 3, 2])

    step1 = tf.matmul(projection_mat, rect_mat)
    step2 = tf.matmul(step1, Tr_velo_cam)
    step2 = tf.tile(tf.expand_dims(step2, axis=1), [1, n_boxes, 1, 1])
    step3 = tf.matmul(step2, corners_in_lidar)
    corners_in_image = tf.transpose(step3, perm=[0, 1, 3, 2])

    z = tf.expand_dims(corners_in_image[:, :, :, 2], axis=3)  # (batch_size, n_boxes, 8, 1)
    corners_in_image = tf.cast((corners_in_image / z), dtype=tf.int32)
    uv = corners_in_image[:, :, :, :2]  # (batch_size, n_boxes, 8, 2)

    horz_pts, vert_pts = tf.split(uv, 2, axis=3)
    horz_pts = tf.clip_by_value(horz_pts, 0, image_hor)
    vert_pts = tf.clip_by_value(vert_pts, 0, image_ver)
    rejoined = tf.concat([horz_pts, vert_pts], axis=3)

    return rejoined