import tensorflow as tf
from tensorflow.keras.layers import Layer


class MiniBatchSampler(Layer):
    def __init__(self, anchor_count=256, pos_fraction=0.5):
        """
        :param anchor_count: int, total anchors needed (256 in Faster-RCNN)
        :param pos_fraction: float, positive anchor percentage (0.5 in Faster-RCNN)
        """
        super().__init__(name='minibatch_sampler')
        self.total = anchor_count
        self.pos_fraction = pos_fraction

    def call(self, inputs, **kwargs):
        """
        Samples n (256) anchors with a fixed pos:neg anchor ratio
        :param inputs: a tuple of the below 3 elements
                        pos_ixs (batch_size, ?) positive anchor indices
                        neg_ixs (batch_size, ?) negative anchor indices
                        gt_box_pos (batch_size, ?, 7) gt_box for positive anchors
        :param kwargs:
        :return:
                        pos_anchors_sampled (batch_size, ?)
                        neg_anchors_sampled (batch_size, ?)
                        gt_box_pos_sampled (batch_size, ?)
        """
        pos_ixs, neg_ixs, gt_box_pos = inputs
        batch_size = tf.shape(pos_ixs)[0]
        n_pos_anc = tf.shape(pos_ixs)[1]
        n_neg_anc = tf.shape(neg_ixs)[1]

        # create ixs_range for pos and neg anchors
        pos_ixs_range = tf.range(start=0, limit=n_pos_anc, delta=1, dtype=tf.int32)
        pos_ixs_range = tf.expand_dims(pos_ixs_range, 0)
        neg_ixs_range = tf.range(start=0, limit=n_neg_anc, delta=1, dtype=tf.int32)
        neg_ixs_range = tf.expand_dims(neg_ixs_range, 0)

        # shuffle the ixs_range
        pos_ixs_range = tf.transpose(pos_ixs_range, [1, 0])
        pos_ixs_range = tf.random_shuffle(pos_ixs_range)
        pos_ixs_range = tf.transpose(pos_ixs_range, [1, 0])
        neg_ixs_range = tf.transpose(neg_ixs_range, [1, 0])
        neg_ixs_range = tf.random_shuffle(neg_ixs_range)
        neg_ixs_range = tf.transpose(neg_ixs_range, [1, 0])

        # pick the top n shuffled pos_ixs_range gather those ixs from pos_ixs & gt_box_pos
        pos_anc_required = int(self.total * self.pos_fraction)
        num_anc_selected = tf.math.minimum(n_pos_anc, pos_anc_required)
        top_shuffled_pos_ixs = tf.slice(pos_ixs_range, [0, 0], [batch_size, num_anc_selected])
        pos_ixs_sampled = tf.squeeze(tf.gather(pos_ixs, top_shuffled_pos_ixs, axis=-1), 0)
        gt_box_pos_sampled = tf.squeeze(tf.gather(gt_box_pos, top_shuffled_pos_ixs, axis=1), 0)

        # pick the top n shuffled neg_ixs_range gather those ixs from neg_ixs
        neg_anc_required = self.total - tf.shape(pos_ixs_sampled)[1]
        top_shuffled_neg_ixs = tf.slice(neg_ixs_range, [0, 0], [batch_size, neg_anc_required])
        neg_ixs_sampled = tf.squeeze(tf.gather(neg_ixs, top_shuffled_neg_ixs, axis=-1), 0)

        return pos_ixs_sampled, neg_ixs_sampled, gt_box_pos_sampled
