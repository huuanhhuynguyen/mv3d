# MultiView 3D Object Detection


    About
    Maintainers
    Contributors
    License
    Content
    Installation
    How to contribute
    Feedback

About

This is a project for implementing MultiView 3D Object Detection for Autonomous Driving. We use KITTI data set for training and evaluation.
See paper: https://arxiv.org/abs/1611.07759

Maintainers

    Huu Anh Huy Nguyen (huuanhhuynguyen@gmail.com)
    Sathya Vasudevan (sav6234@thi.de)

Contributors

    -

License

    -

Content

    ./kitti_mini_dataset: a small set of the kitti data set (www.cvlibs.net/datasets/kitti/eval_object.php?obj_benchmark=3d) for development purpose.
    ./pipeline: source code of the whole pipeline (e.g. BEV input representation, RPN)
    ./third_party: 3rd-party code
    ./training: scripts for running training and evaluation
    ./visualization: scripts and saved images for visualisation.
    ./test: unit tests (pytest)
    
Visualisation

![picture](visualisation/imgs/FV.png)

![picture](visualisation/imgs/BEV.png)

![picture](visualisation/imgs/camera_view_3D.png)

Installation

	Clone this project:

    git clone https://huuanhhuynguyen@bitbucket.org/huuanhhuynguyen/mv3d.git

    Create a new conda environment and activate it

    conda create -n mv3d
    source activate mv3d (or conda activate mv3d)
    
    cd <project_source_path>
    conda install --file ./requirements.txt
    pip install -e .

How to contribute

    Your contribution is very welcomed. To contribute, please create a feature branch - for instance feature\improve_readme - and apply your changes.

    Please, make sure you have considered the following aspects before creating a pull request:

    Increment the minor version z of the python package version x.y.z specified in the setup.py.

    Make sure all unit tests of the package run successfully.

    If your code contains snippets copied from third party code, please reference the source and its license in your code

    When creating the pull request your must add at least one of the Maintainers or Contributors as reviewer. We will discuss your suggested changes with the goal to merge these changes in the master branch.

Feedback

    We highly advocate feedbacks. Please contact the Maintainer(s).