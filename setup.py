from setuptools import setup, find_packages


with open('pip_requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='mv3d',
    version='0.0.2',
    packages=find_packages(),
    license='MIT License',
    description='Implementation of the paper MultiView 3D Object '
                'Detection for Autonomous Driving',
    url='https://bitbucket.org/huuanhhuynguyen/mv3d/src/',
    author='Huy Nguyen',
    author_email='huuanhhuynguyen@gmail.com',
    install_requires=requirements,
    python_requires='>=3.6.0'
)