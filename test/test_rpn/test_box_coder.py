import numpy as np
import tensorflow as tf

from pipeline.rpn import BoxCoder


def test_box_coder():
    n_anchors = n_boxes = 20

    # get random gt boxes
    theta = np.random.rand(n_boxes, 1)
    theta = np.arctan2(np.cos(theta), np.sin(theta))
    x = np.random.rand(n_boxes, 1)
    y = np.random.rand(n_boxes, 1)
    z = np.random.rand(n_boxes, 1)
    l = np.random.rand(n_boxes, 1)
    w = np.random.rand(n_boxes, 1)
    h = np.random.rand(n_boxes, 1)
    boxes = np.concatenate([theta, x, y, z, l, w, h], axis=-1)

    # get random anchors
    anchors = np.random.rand(n_anchors, 7)

    boxes_tf = tf.convert_to_tensor(boxes)
    anchors_tf = tf.convert_to_tensor(anchors)

    # encode boxes and decode again
    codes = BoxCoder.encode(boxes_tf, anchors_tf)
    decodes = BoxCoder.decode(codes, anchors_tf)

    with tf.Session() as sess:
        decoded_boxes = sess.run(decodes)

    # after decoding, the returned boxes must be the same
    np.testing.assert_almost_equal(boxes[:, 1:7], decoded_boxes[:, 1:7])
    # theta is not regressed, so it should match with original anchor value
    np.testing.assert_almost_equal(anchors[:, 0], decoded_boxes[:, 0])