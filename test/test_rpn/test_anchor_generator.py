import numpy as np
import tensorflow as tf

from pipeline.rpn import AnchorGenerator
from pipeline.utils.coordinates import X, Y


def test_anchor_generator():
    batch_size = 1
    feature_maps = np.random.rand(batch_size, int(X/4), int(Y/4), 5)
    feature_maps = tf.convert_to_tensor(feature_maps, dtype=tf.float32)

    anchor_gen = AnchorGenerator()
    anchors = anchor_gen(feature_maps)

    with tf.Session() as sess:
        _, n_anchors, n_anchor_param = anchors.shape
        K = 1
        assert n_anchors == X/4 * Y/4 * K
        assert n_anchor_param == 7
        anchors = sess.run(anchors)