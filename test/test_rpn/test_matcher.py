import numpy as np
from math import pi, sqrt
import tensorflow as tf

from pipeline.utils._numpy_version import matcher_np, distance_calculator_np
from pipeline.rpn import Matcher, DistanceCalculator


def to_3D_boxes(boxes2d):
    theta, x, y, l, w = np.split(boxes2d, 5, axis=-1)
    h = z = np.ones_like(x) * 42  # dummy values
    boxes3d = np.concatenate([theta, x, y, z, l, w, h], axis=-1)
    return boxes3d


def test_matcher():

    # x, y, theta
    anchors = np.array([[0, 1.5, 1.5, -1, 4, 2, 1.6], [0, -1, -1, -1, 4, 2, 1.6],
                        [0, 2, -2, -1, 4, 2, 1.6], [0, -2, 2, -1, 4, 2, 1.6],
                        [0, 100, 100, -1, 4, 2, 1.6], [0, 150, 150, -1, 4, 2, 1.6],
                        [0, 200, 200, -1, 4, 2, 1.6], [0, 250, 250, -1, 4, 2, 1.6]], dtype=np.float32)

    # theta, x, y, l, w
    eps = 0.001
    gt_boxes_2d = np.array([[0, 2, 1.5, 2, 1],
                            [-pi/4-eps, 2, -2, sqrt(2), sqrt(2)],
                            [pi/2, -1.5, -2, 2, 1],
                            [3*pi/4+eps, -1.5, 1.5, sqrt(8), sqrt(2)]], dtype=np.float32)
    gt_boxes_3d = to_3D_boxes(gt_boxes_2d)

    # expected values
    expected_pos_anchors = np.array([[0, 1.5, 1.5, -1, 4, 2, 1.6], [0, -1, -1, -1, 4, 2, 1.6],
                                     [0, 2, -2, -1, 4, 2, 1.6], [0, -2, 2, -1, 4, 2, 1.6]], dtype=np.float32)
    expected_neg_anchors = np.array([[0, 100, 100, -1, 4, 2, 1.6], [0, 150, 150, -1, 4, 2, 1.6],
                                     [0, 200, 200, -1, 4, 2, 1.6], [0, 250, 250, -1, 4, 2, 1.6]], dtype=np.float32)
    expected_gt_boxes = np.array([gt_boxes_3d[0], gt_boxes_3d[2], gt_boxes_3d[1], gt_boxes_3d[3]])

    # simulate a batch of two samples
    anchors = np.tile(anchors[None, ...], [2, 1, 1])
    gt_boxes_3d = np.tile(gt_boxes_3d[None, ...], [2, 1, 1])
    expected_pos_anchors = np.tile(expected_pos_anchors[None, ...], [2, 1, 1])
    expected_neg_anchors = np.tile(expected_neg_anchors[None, ...], [2, 1, 1])
    expected_gt_boxes = np.tile(expected_gt_boxes[None, ...], [2, 1, 1])

    # match
    pos_anchors_ixs, neg_anchors_ixs, gt_boxes_pos = matcher_np(anchors, gt_boxes_3d)
    pos_anchors = np.array(tuple(a[p] for a, p in zip(anchors, pos_anchors_ixs)))
    neg_anchors = np.array(tuple(a[n] for a, n in zip(anchors, neg_anchors_ixs)))

    # asserts
    np.testing.assert_almost_equal(pos_anchors, expected_pos_anchors)
    np.testing.assert_almost_equal(neg_anchors, expected_neg_anchors)
    np.testing.assert_almost_equal(gt_boxes_pos, expected_gt_boxes)

    # tensor
    anchors_tf = tf.convert_to_tensor(anchors)
    gt_boxes_3d_tf = tf.convert_to_tensor(gt_boxes_3d)
    matcher = Matcher()

    def gather(inputs):
        params, ixs = inputs
        return tf.gather(params, ixs)

    # match
    with tf.Session() as sess:
        pos_anchors_ixs, neg_anchors_ixs, gt_boxes_pos = matcher([anchors_tf, gt_boxes_3d_tf])
        pos_anchors = tf.map_fn(gather, [anchors, pos_anchors_ixs], dtype=tf.float32)
        neg_anchors = tf.map_fn(gather, [anchors, neg_anchors_ixs], dtype=tf.float32)
        pos_anchors = sess.run(pos_anchors)
        neg_anchors = sess.run(neg_anchors)
        gt_boxes_pos = sess.run(gt_boxes_pos)

    # asserts
    np.testing.assert_almost_equal(pos_anchors, expected_pos_anchors)
    np.testing.assert_almost_equal(neg_anchors, expected_neg_anchors)
    np.testing.assert_almost_equal(gt_boxes_pos, expected_gt_boxes)


def test_distance_calculator():
    # x, y, theta
    anchors = np.array([[-pi, 2, 2, -1, 4, 2, 1.6], [-pi / 2, 2, -2, -1, 4, 2, 1.6],
                        [0, -2, 2, -1, 4, 2, 1.6], [pi / 2, -2, -2, -1, 4, 2, 1.6]], dtype=np.float32)

    # theta, x, y, l, w
    gt_boxes_2d = np.array([[0, 1.5, 1.5, 2, 1],
                            [-pi/4, -1.5, -1.5, sqrt(2), sqrt(2)],
                            [pi/2, 1.5, -1.5, 2, 1],
                            [3*pi/4, -1.5, 1.5, sqrt(8), sqrt(2)]], dtype=np.float32)

    # expected values
    expected_dist_mat = np.array([
        [np.sqrt(0.5*0.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 3.5*3.5), np.sqrt(3.5*3.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 0.5*0.5)],
        [np.sqrt(3.5*3.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 0.5*0.5), np.sqrt(0.5*0.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 3.5*3.5)],
        [np.sqrt(3.5*3.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 3.5*3.5), np.sqrt(0.5*0.5 + 0.5*0.5)],
        [np.sqrt(3.5*3.5 + 3.5*3.5), np.sqrt(0.5*0.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 0.5*0.5), np.sqrt(3.5*3.5 + 0.5*0.5)]
    ], dtype=np.float32)

    # simulate a batch of two samples
    anchors = np.tile(anchors[None, ...], [2, 1, 1])
    gt_boxes_2d = np.tile(gt_boxes_2d[None, ...], [2, 1, 1])
    expected_dist_mat = np.tile(expected_dist_mat[None, ...], [2, 1, 1])

    # calc distance
    dist_mat = distance_calculator_np(anchors[:, :, 1:3], gt_boxes_2d)

    # assert
    np.testing.assert_almost_equal(dist_mat, expected_dist_mat)

    # tensor
    anchors_tf = tf.convert_to_tensor(anchors)
    gt_boxes_2d_tf = tf.convert_to_tensor(gt_boxes_2d)
    distance_calculator = DistanceCalculator()

    # calc distance
    with tf.Session() as sess:
        dist_mat_tf = distance_calculator([anchors_tf, gt_boxes_2d_tf])
        dist_mat_tf = sess.run(dist_mat_tf)

    # assert
    np.testing.assert_almost_equal(dist_mat_tf, expected_dist_mat, decimal=6)