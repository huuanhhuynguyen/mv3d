import tensorflow as tf
import numpy as np

from pipeline.mini_batch_sampling import MiniBatchSampler

def test_sampler():
    pos_ixs = tf.constant([[1, 2, 3, 4, 5, 6]], dtype=tf.int32)
    neg_ixs = tf.constant([[9, 10, 11, 18, 19, 24, 28, 32, 40, 41, 45, 50, 65, 68, 76, 78]], dtype=tf.int32)
    gt_boxes_pos = tf.constant([[[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                                [2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0],
                                [3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0],
                                [4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0],
                                [5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
                                [6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0]]])

    mini_batch_sampler = MiniBatchSampler(anchor_count=14, pos_fraction=0.5)
    pos_ixs_sampled, neg_ixs_sampled, gt_box_sampled = mini_batch_sampler([pos_ixs, neg_ixs, gt_boxes_pos])

    with tf.Session() as sess:
        pos_ixs_sampled, neg_ixs_sampled, gt_box_sampled = sess.run([pos_ixs_sampled, neg_ixs_sampled, gt_box_sampled])

    pos_ixs_sampled = np.float32(pos_ixs_sampled)

    np.testing.assert_almost_equal(pos_ixs_sampled[0, :], gt_box_sampled[0, :, 0])
    assert pos_ixs_sampled.shape[1] == 6
    assert neg_ixs_sampled.shape[1] == 14-6
