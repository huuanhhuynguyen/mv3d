import tensorflow as tf
import numpy as np
from pipeline.roi_pooling import ROIPool


def test_roi_pooling():

    input = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 4, 0, 0, 8, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 2, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0, 8]], dtype=np.float32)

    input = input[None, ..., None]
    input_tf = tf.convert_to_tensor(input, dtype=tf.float32)

    roi_tf = tf.reshape(tf.constant([0, 2, 2, 7, 7], dtype=tf.float32), [-1, 5])

    roipooling = ROIPool(4, 4, 1.0)
    pooled_roi, _ = roipooling([input_tf, roi_tf])

    with tf.Session() as sess:
        pooled_roi = sess.run(pooled_roi)

    pooled_roi = np.squeeze(pooled_roi, 0)
    pooled_roi = np.squeeze(pooled_roi, 2)

    expected_array = np.array([[4, 0, 8, 0],
                              [0, 0, 0, 0],
                              [2, 0, 0, 0],
                              [0, 0, 0, 8]], dtype=np.float32)

    np.testing.assert_almost_equal(pooled_roi, expected_array)