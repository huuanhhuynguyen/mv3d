from pipeline.data_gen import DataGenerator


def test_single_batch():
    data_gen = DataGenerator()
    (bev, boxes3d), _ = data_gen[1]
    assert bev.shape[0] == 1 and len(bev.shape) == 4
    assert boxes3d.shape[0] == 1 and len(boxes3d.shape) == 3

