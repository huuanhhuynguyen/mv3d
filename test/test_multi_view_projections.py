from pipeline.multi_view_projections import box3d_to_bev, box3d_to_fv, \
    _lidar_to_fv_coords, box3d_to_camera2d, box3d_to_camera3d

from pipeline.multi_view_projections import box3d_to_bev_tf, box3d_to_fv_tf, \
    box3d_to_camera2d_tf, box3d_to_camera3d_tf

import numpy as np
import tensorflow as tf


box_3d = np.array([[[8, 0.5, 0], [8, -0.5, 0], [12, -0.5, 0], [12, 0.5, 0],
                   [8, 0.5, -2], [8, -0.5, -2], [12, -0.5, -2], [12, 0.5, -2]]])
box_3d_tf = tf.convert_to_tensor(box_3d, dtype=tf.float64) # (1, 8, 3)


Tr_velo_cam = np.array([[0, -1, 0, 0],
                        [0, 0, -1, 0],
                        [1, 0, 0, 0],
                        [0, 0, 0, 1]])
Tr_velo_cam_tf = tf.convert_to_tensor(Tr_velo_cam, dtype=tf.float64)
Tr_velo_cam_tf = tf.expand_dims(Tr_velo_cam_tf, 0) # (1, 4, 4)


P2 = np.array([[721, 0, 609, 0],
               [0, 962, 230, 0],
               [0,   0,   1, 0]])
P2_tf = tf.convert_to_tensor(P2, dtype=tf.float64)
P2_tf = tf.expand_dims(P2_tf, 0) # (1, 3, 4)


R0 = np.array([[1, 0, 0, 0],
               [0, 1, 0, 0],
               [0, 0, 1, 0],
               [0, 0, 0, 1]])
R0_tf = tf.convert_to_tensor(R0, dtype=tf.float64)
R0_tf = tf.expand_dims(R0_tf, 0) # (1, 4, 4)


def test_box3d_to_bev():
    bev_boxes = box3d_to_bev(box_3d) # numpy
    bev_boxes_tf = box3d_to_bev_tf(tf.expand_dims(box_3d_tf, 0)) # tf

    with tf.Session() as sess:
        bev_boxes_tf = sess.run(bev_boxes_tf)

    bev_boxes_tf = np.squeeze(bev_boxes_tf, 0) # to match numpy result

    expected_array = np.array([[[623, 394], [623, 404], [583, 404], [583, 394]]])

    # assertions
    np.testing.assert_array_equal(bev_boxes, expected_array)
    np.testing.assert_almost_equal(bev_boxes_tf, bev_boxes)
    assert np.min(bev_boxes[:, 0]) >= 0 and np.max(bev_boxes[:, 0]) <= 704
    assert np.min(bev_boxes[:, 1]) >= 0 and np.max(bev_boxes[:, 1]) <= 800


def test_box3d_to_fv():
    fv = box3d_to_fv(box_3d) # numpy
    fv_tf = box3d_to_fv_tf(tf.expand_dims(box_3d_tf, 0)) # tf

    with tf.Session() as sess:
        fv_tf = sess.run(fv_tf)

    fv_tf = np.squeeze(fv_tf, 0) # to match numpy result

    expected_array = np.array([[[235, 2], [276, 2], [276, 37], [235, 37]]])

    # assertions
    np.testing.assert_array_equal(fv, expected_array)
    np.testing.assert_almost_equal(fv, fv_tf)
    assert np.min(fv[:, :, 0]) >= 0 and np.max(fv[:, :, 0]) <= 512
    assert np.min(fv[:, :, 1]) >= 0 and np.max(fv[:, :, 1]) <= 64


def test_lidar_to_fv_coords():
    x, y, z = 8, 0.5, 0

    hor, ver = _lidar_to_fv_coords(x, y, z)

    assert hor == 235
    assert ver == 2


def test_box3d_to_camera2d():
    roi = box3d_to_camera2d(box_3d, Tr_velo_cam, P2, R0) # numpy
    roi_tf = box3d_to_camera2d_tf(tf.expand_dims(box_3d_tf, 0), Tr_velo_cam_tf, P2_tf, R0_tf) # tf

    with tf.Session() as sess:
        roi_tf = sess.run(roi_tf)

    roi_tf = np.squeeze(roi_tf, 0) # to match numpy result

    expected_array = np.array([[[563, 230], [654, 230], [654, 375], [563, 375]]])

    # assertions
    np.testing.assert_array_equal(roi, expected_array)
    np.testing.assert_almost_equal(roi_tf, roi)


def test_box3d_to_camera3d():
    roi_3d = box3d_to_camera3d(box_3d, Tr_velo_cam, P2, R0)
    roi_3d_tf = box3d_to_camera3d_tf(tf.expand_dims(box_3d_tf, 0), Tr_velo_cam_tf, P2_tf, R0_tf)

    with tf.Session() as sess:
        roi_3d_tf = sess.run(roi_3d_tf)

    roi_3d_tf = np.squeeze(roi_3d_tf, 0)

    expected_array = np.array([[[563, 230], [654, 230], [639, 230], [578, 230],
                                [563, 375], [654, 375], [639, 375], [578, 375]]])

    # assertions
    np.testing.assert_array_equal(roi_3d, expected_array)
    np.testing.assert_almost_equal(roi_3d_tf, roi_3d)