from os.path import dirname
import numpy as np
from math import pi, sqrt
import tensorflow as tf

from pipeline.utils.coordinates import get_corners_bev, get_corners_3D, get_bev, to_KITTI, to_ISO, to_bev_boxes, \
    get_corners_bev_tf, get_corners_3D_tf, to_KITTI_tf, to_ISO_tf, reformat_rois, to_bev_boxes_tf, convert_to_diagonal_corners

from pipeline.utils.read import read_velo

batch_size = 1


def test_get_corners_bev():
    # theta, cx, cy, l, w
    obb1 = [0, 2, 0.5, 2, 1]
    obb2 = [-pi/4, 2, -2, sqrt(2), sqrt(2)]
    obb3 = [-pi, -2, -1.5, 2, 1]
    obb4 = [-3*pi/4, -2, 2, sqrt(2), sqrt(2)]
    obbs = np.stack([obb1, obb2, obb3, obb4], axis=0)

    corners = get_corners_bev(obbs)

    # tensors
    obb1 = tf.convert_to_tensor(obb1)
    obb2 = tf.convert_to_tensor(obb2)
    obb3 = tf.convert_to_tensor(obb3)
    obb4 = tf.convert_to_tensor(obb4)
    obbs = tf.stack([obb1, obb2, obb3, obb4], axis=0)
    obbs = tf.tile(tf.expand_dims(obbs, 0), [batch_size, 1, 1])

    corners_tf = get_corners_bev_tf(obbs)

    with tf.Session() as sess:
        corners_tf = sess.run(corners_tf)

    corners_tf = np.squeeze(corners_tf, 0)

    np.testing.assert_almost_equal(corners, corners_tf)
    np.testing.assert_almost_equal(corners[0], np.array([(3, 1), (3, 0), (1, 0), (1, 1)]))
    np.testing.assert_almost_equal(corners[1], np.array([(3, -2), (2, -3), (1, -2), (2, -1)]))
    np.testing.assert_almost_equal(corners[2], np.array([(-1, -1), (-1, -2), (-3, -2), (-3, -1)]))
    np.testing.assert_almost_equal(corners[3], np.array([(-2, 3), (-1, 2), (-2, 1), (-3, 2)]))


def test_get_corners_3D():
    # theta, cx, cy, cz, l, w, h
    obb1 = [0, 2, 0.5, 1, 2, 1, 2]
    obb2 = [-pi/4, 2, -2, 1, sqrt(2), sqrt(2), 2]
    obb3 = [-pi, -2, -1.5, 1, 2, 1, 2]
    obb4 = [-3*pi/4, -2, 2, 1, sqrt(2), sqrt(2), 2]
    obbs = np.stack([obb1, obb2, obb3, obb4], axis=0)

    corners = get_corners_3D(obbs, forward_backward_equivalence=True)

    # tf
    obb1 = tf.convert_to_tensor(obb1)
    obb2 = tf.convert_to_tensor(obb2)
    obb3 = tf.convert_to_tensor(obb3)
    obb4 = tf.convert_to_tensor(obb4)
    obbs = tf.stack([obb1, obb2, obb3, obb4], axis=0)
    obbs = tf.tile(tf.expand_dims(obbs, 0), [batch_size, 1, 1])

    corners_tf = get_corners_3D_tf(obbs, forward_backward_equivalence=True)

    with tf.Session() as sess:
        corners_tf = sess.run(corners_tf)

    corners_tf = np.squeeze(corners_tf, 0) # to match numpy dimensions


    np.testing.assert_almost_equal(corners, corners_tf)
    np.testing.assert_almost_equal(corners[0], np.array([(3, 1, 1), (3, 0, 1), (1, 0, 1), (1, 1, 1),
                                                         (1, 1, 3), (3, 1, 3), (3, 0, 3), (1, 0, 3)]))

    np.testing.assert_almost_equal(corners[1], np.array([(3, -2, 1), (2, -3, 1), (1, -2, 1), (2, -1, 1),
                                                         (2, -1, 3), (3, -2, 3), (2, -3, 3), (1, -2, 3)]))

    np.testing.assert_almost_equal(corners[2], np.array([(-1, -1, 1), (-1, -2, 1), (-3, -2, 1), (-3, -1, 1),
                                                         (-3, -1, 3), (-1, -1, 3), (-1, -2, 3), (-3, -2, 3)]))

    np.testing.assert_almost_equal(corners[3], np.array([(-2, 3, 1), (-1, 2, 1), (-2, 1, 1), (-3, 2, 1),
                                                         (-3, 2, 3), (-2, 3, 3), (-1, 2, 3), (-2, 1, 3)]))


def test_get_bev():
    project_dir = dirname(dirname(dirname(__file__)))
    frame = read_velo(0)
    _ = get_bev(frame, x_min=-50, x_max=50)


def test_coordinate_transformation():
    x = np.array([1, 2, 3]).reshape(-1, 1)
    y = np.array([4, 5, 6]).reshape(-1, 1)
    z = np.array([7, 8, 9]).reshape(-1, 1)
    thetaY = np.array([10, 11, 12]).reshape(-1, 1)

    Tr_velo_cam = np.array([[0, -1, 0, 0],
                            [0, 0, -1, 0],
                            [1, 0, 0, 0],
                            [0, 0, 0, 1]])

    points_iso = to_ISO(Tr_velo_cam, points3d=np.concatenate([x, y, z], axis=1))
    theta = to_ISO(Tr_velo_cam, thetaY=thetaY)
    points_kitti = to_KITTI(Tr_velo_cam, points3d=points_iso)
    thetak = to_KITTI(Tr_velo_cam, thetaZ=theta)

    np.testing.assert_almost_equal(np.concatenate([x, y, z], axis=1), points_kitti)

    # convert to tensors
    x = tf.convert_to_tensor(x, dtype=tf.float32)
    y = tf.convert_to_tensor(y, dtype=tf.float32)
    z = tf.convert_to_tensor(z, dtype=tf.float32)
    thetaY_tf = tf.convert_to_tensor(thetaY, dtype=tf.float32)
    Tr_velo_cam = tf.convert_to_tensor(Tr_velo_cam, dtype=tf.float32)

    points_iso = to_ISO_tf(Tr_velo_cam, points3d=tf.concat([x, y, z], axis=1))
    theta = to_ISO_tf(Tr_velo_cam, thetaY=thetaY_tf)

    points_kitti_tf = to_KITTI_tf(Tr_velo_cam, points3d=points_iso)
    thetak_tf = to_KITTI_tf(Tr_velo_cam, thetaZ=theta)


    with tf.Session() as sess:
        points_kitti_tf = sess.run(points_kitti_tf)
        thetaY_tf = sess.run(thetaY_tf)
        thetak_tf = sess.run(thetak_tf)


    np.testing.assert_almost_equal(points_kitti_tf, points_kitti)
    np.testing.assert_almost_equal(thetaY_tf, thetaY)
    np.testing.assert_almost_equal(thetak_tf, thetak)
    np.testing.assert_almost_equal(thetaY, thetak)


def test_to_bev_boxes():
    boxes3d = np.random.rand(1, 7)
    boxes2d = to_bev_boxes(boxes3d)
    assert boxes2d.shape == (1, 5)
    with tf.Session() as sess:
        boxes3d_tf = tf.expand_dims(tf.convert_to_tensor(boxes3d), 0)
        boxes2d = sess.run(to_bev_boxes_tf(boxes3d_tf))
        assert boxes2d.shape == (1, 1, 5)


def test_reformat_rois():
    box = tf.constant([[100, 20],
                        [200, 30],
                        [240, 80],
                        [80, 100]], dtype=tf.float32)

    n_boxes = 2
    n_batches = 3
    box = tf.tile(tf.expand_dims(box, 0), [n_boxes, 1, 1])
    box = tf.tile(tf.expand_dims(box, 0), [n_batches, 1, 1, 1])

    reformatted_rois = reformat_rois(box) # (batch_id, y1, x1, y2, x2)

    with tf.Session() as sess:
        reformatted_rois = sess.run(reformatted_rois)

    expected_array = np.array([[0.0, 20.0, 80.0, 100.0, 240.0],
                               [0.0, 20.0, 80.0, 100.0, 240.0],
                               [1.0, 20.0, 80.0, 100.0, 240.0],
                               [1.0, 20.0, 80.0, 100.0, 240.0],
                               [2.0, 20.0, 80.0, 100.0, 240.0],
                               [2.0, 20.0, 80.0, 100.0, 240.0]])

    np.testing.assert_almost_equal(reformatted_rois, expected_array)


def test_diag_corners():
    box = tf.constant([[100, 20],
                       [200, 30],
                       [240, 80],
                       [80, 100]], dtype=tf.float32)

    n_boxes = 2
    n_batches = 3
    box = tf.tile(tf.expand_dims(box, 0), [n_boxes, 1, 1])
    box_batch = tf.tile(tf.expand_dims(box, 0), [n_batches, 1, 1, 1])

    diag_corners = convert_to_diagonal_corners(box)
    diag_corners_batch = convert_to_diagonal_corners(box_batch)

    with tf.Session() as sess:
        diag_corners = sess.run(diag_corners)
        diag_corners_batch = sess.run(diag_corners_batch)

    expected_array = np.array([[80.0, 20.0, 240.0, 100.0],
                               [80.0, 20.0, 240.0, 100.0]])
    expected_array_batch = np.array([[80.0, 20.0, 240.0, 100.0],
                                     [80.0, 20.0, 240.0, 100.0]])
    expected_array_batch = np.tile(np.expand_dims(expected_array_batch, 0), [3, 1, 1])


    np.testing.assert_almost_equal(diag_corners, expected_array)
    np.testing.assert_almost_equal(diag_corners_batch, expected_array_batch)
