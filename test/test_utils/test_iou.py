import numpy as np
from pipeline.utils.iou import iou_rtree, iou_np


def gen_boxes(n_boxes):
    theta = np.random.uniform(-np.pi, np.pi, (n_boxes,))
    cx = np.random.uniform(0, 70, (n_boxes,))
    cy = np.random.uniform(-40, 40, (n_boxes,))
    l = np.random.uniform(2, 4, (n_boxes,))
    w = np.random.uniform(1, 2, (n_boxes,))
    return np.array([theta, cx, cy, l, w]).T


def test_iou():

    boxesA = gen_boxes(n_boxes=200)
    boxesB = gen_boxes(n_boxes=15)

    iou_method1 = iou_np(boxesA, boxesB)
    iou_method2 = iou_rtree(boxesA, boxesB)
    np.testing.assert_almost_equal(iou_method1, iou_method2, decimal=9)
