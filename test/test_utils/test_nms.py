import tensorflow as tf
import numpy as np
from pipeline.nms import NonMaxSuppression


def test_nms():

    boxes = np.array([[10.0, 10.0, 40.0, 40.0], [11.0, 11.0, 41.0, 41.0], [20.0, 20.0, 50.0, 50.0]])
    scores = np.array([0.88, 0.85, 0.9])

    boxes_tf = tf.expand_dims(tf.convert_to_tensor(boxes, dtype=tf.float32), 0)
    scores_tf = tf.expand_dims(tf.convert_to_tensor(scores, dtype=tf.float32), 0)

    non_max_suppression = NonMaxSuppression(n_boxes_out=2, iou=0.5)
    idx = non_max_suppression([boxes_tf, scores_tf])

    def gather(inputs):
        params, ixs, axis = inputs
        return tf.gather(params, ixs, axis=axis)

    axis_val = tf.expand_dims(tf.convert_to_tensor(0), 0)
    selected_boxes = tf.map_fn(gather, [boxes_tf, idx, axis_val], dtype=tf.float32)

    with tf.Session() as sess:
        selected_boxes = sess.run(selected_boxes)

    expected_boxes = np.array([[[20.0, 20.0, 50.0, 50.0], [10.0, 10.0, 40.0, 40.0]]])
    np.testing.assert_almost_equal(selected_boxes, expected_boxes)
