import numpy as np
from pipeline.utils.plot import plot_obbs_bev, plot_3D_obbs_camview


def test_plot_obbs_bev():
    import matplotlib.pyplot as plt

    corners1 = [(1, 0), (3, 0), (3, 1), (1, 1)]
    corners2 = [(-1, 0), (0, 1), (-1, 2), (-2, 1)]

    plt = plot_obbs_bev(plt, np.array([corners1, corners2]))
    plt.axis('equal')
    plt.grid()
    plt.xlabel('-y direction')
    plt.ylabel('+x direction')
    #plt.show()


def test_plot_3D_obbs_camview():
    import matplotlib.pyplot as plt

    corners1 = [(1, 0), (3, 0), (4, 1), (2, 1),
                (2, 3), (1, 2), (3, 2), (4, 3)]
    corners2 = [(-1, 0), (0, 1), (-1, 2), (-2, 1),
                (-2, 3), (-1, 2), (0, 3), (-1, 4)]

    plt = plot_3D_obbs_camview(plt, np.array([corners1, corners2]))
    plt.axis('equal')
    plt.grid()
    plt.xlabel('-y direction')
    plt.ylabel('+x direction')
    #plt.show()