from pipeline.input_representation import BEVRepresentation, FVRepresentation
from math import log
import numpy as np
from numpy.random import uniform


def test_encode_bev():
    bev_rep = BEVRepresentation(x_range=(0, 0.5001), y_range=(-0.2, 0.5001), z_range=(0, 0.3001))

    # x, y, z, refl
    lidar_data = [
        [0.15, 0.05, 0.05, 15],  # cell = (1, 2, 0), height=0.05, ref = 15
        [0.15, 0.05, 0.06, 14],  # cell = (1, 2, 0), height=0.06, ref = 14
        [0.15, 0.05, 0.07, 13],  # cell = (1, 2, 0), height=0.07, ref = 13

        [0.15, 0.05, 0.15, 25],  # cell = (1, 2, 1), height=0.15, ref = 25
        [0.15, 0.05, 0.17, 24],  # cell = (1, 2, 1), height=0.17, ref = 24
        [0.15, 0.05, 0.16, 23],  # cell = (1, 2, 1), height=0.16, ref = 23

        [0.15, 0.05, 0.4, 30]  # cell = (1, 2, 4), out of range point
    ]

    bev = bev_rep.encode(lidar_data, dz=0.1)
    assert bev.shape == (5, 7, 3+2)  # (X, Y, M+2)

    X, Y = 5, 7  # for flipping the x,y-indices
    # Check height encoding (M=4)
    assert bev[X-1][Y-2][0] == 0.07
    assert bev[X-1][Y-2][1] == 0.17
    assert bev[X-1][Y-2][2] == 0.0  # default

    # Check intensity encoding
    assert bev[X-1][Y-2][3] == 24  # intensity (i.e. reflectance of the point with max height in a pillar)

    # Check density encoding
    n_points_in_pillar = 6
    assert bev[X-1][Y-2][4] == log(n_points_in_pillar+1)/log(64)  # density


def test_encode_bev_in_batch():
    bev_rep = BEVRepresentation(x_range=(0, 0.5001), y_range=(-0.2, 0.5001), z_range=(0, 0.3001))

    # x, y, z, refl
    lidar_data = [
        [0.15, 0.05, 0.05, 15],  # cell = (1, 2, 0), height=0.05, ref = 15
        [0.15, 0.05, 0.06, 14],  # cell = (1, 2, 0), height=0.06, ref = 14
        [0.15, 0.05, 0.07, 13],  # cell = (1, 2, 0), height=0.07, ref = 13

        [0.15, 0.05, 0.15, 25],  # cell = (1, 2, 1), height=0.15, ref = 25
        [0.15, 0.05, 0.17, 24],  # cell = (1, 2, 1), height=0.17, ref = 24
        [0.15, 0.05, 0.16, 23],  # cell = (1, 2, 1), height=0.16, ref = 23

        [0.15, 0.05, 0.4, 30]  # cell = (1, 2, 4), out of range point
    ]

    batch_size = 3
    lidar_data = np.array(lidar_data)
    lidar_data = np.tile(lidar_data[None, ...], [batch_size, 1, 1])
    bev = bev_rep.encode(lidar_data, dz=0.1)
    assert bev.shape == (batch_size, 5, 7, 3+2)


def test_encode_fv():
    enc_fv = FVRepresentation()
    lidar_data = []
    for _ in range(1000):
        x = uniform(0.0, 70.2)
        y = uniform(-40.0, 40.0)
        z = uniform(0.0, 10.0)
        refl = uniform(0.0, 1.0)
        lidar_data.append([x, y, z, refl])

    fv = enc_fv.encode(lidar_data)
    assert fv.shape == (64, 512, 3)


def test_encode_fv_in_batch():
    enc_fv = FVRepresentation()
    batch_size = 3
    lidar_data_batch=[]
    for _ in range(batch_size):
        lidar_data = []
        for _ in range(1000):
            x = uniform(0.0, 70.2)
            y = uniform(-40.0, 40.0)
            z = uniform(0.0, 10.0)
            refl = uniform(0.0, 1.0)
            lidar_data.append([x, y, z, refl])
        lidar_data_batch.append(lidar_data)

    fv = enc_fv.encode(lidar_data_batch)
    assert fv.shape == (batch_size, 64, 512, 3)

