from pipeline.model import MV3D
from pipeline.data_gen import DataGenerator
from tensorflow import keras
import argparse
import yaml
from os.path import dirname, join


def train():
    model = MV3D()
    yaml_path = join(dirname(__file__), args.yaml_name)

    with open(yaml_path) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    # Build the model
    default_bev = keras.Input(shape=(704, 800, 7))
    default_gt_boxes = keras.Input(shape=(1, 7))
    model([default_bev, default_gt_boxes])

    model.compile(optimizer=config['optimizer'],
                  loss=[None, None])

    train_data_generator = DataGenerator()
    valid_data_generator = DataGenerator()
    model.fit_generator(generator=train_data_generator,
                        validation_data=valid_data_generator,
                        use_multiprocessing=True,
                        epochs=config['epochs'],
                        workers=5,
                        shuffle=False,
                        callbacks=[],
                        initial_epoch=0,
                        verbose=True)


if __name__ == '__main__':
    # TODO load yaml config (data_folder, train hyperparams)
    # TODO data loading with tf.data (iterators, etc.)
    # TODO command line args
    # TODO visualize results (nms and k-boxes)
    # TODO save and load checkpoints
    # TODO data loader
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='config', type=str,
                        help="Name of the yaml config file")

    args = parser.parse_args()
    train()
